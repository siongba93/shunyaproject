﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlayerUpDieCheckController : PlayerStateController {

	public PlayerMainController controller;

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground && PlayerIsGround) {
			controller.playerDie ();
		}
	}
}