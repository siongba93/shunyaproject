﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackWhiteDataBase : MonoBehaviour {
	public float BlackWhiteLeft;
	public float BlackWhiteRight;
	public float BlackWhiteUp;
	public float BlackWhiteDown;

	public float prefabX;
	public float prefabY;

	public GameObject Prefab;
	public BlackWhiteSquareController WhiteController;
	public BlackWhiteSquareController BlackController;
	public Vector2 middle;

	public Vector2 storeData = Vector2.zero;
	public GameObject storeSquare;

	void Update () {
		if (WhiteController == null || BlackController == null) {
			Destroy (storeSquare);
			return;
		}

		#region White Controller
		if (WhiteController.Down.Inside == true && BlackController.Down.Inside == false) {
			BlackWhiteDown = WhiteController.Down.myBody.position.y;
		}
		if (WhiteController.Up.Inside == true && BlackController.Up.Inside == false) {
			BlackWhiteUp = WhiteController.Up.myBody.position.y;
		}
		if (WhiteController.Right.Inside == true && BlackController.Right.Inside == false) {
			BlackWhiteRight = WhiteController.Right.myBody.position.x;
		}
		if (WhiteController.Left.Inside == true && BlackController.Left.Inside == false) {
			BlackWhiteLeft = WhiteController.Left.myBody.position.x;
		}
		#endregion

		#region Black Controller
		if (WhiteController.Down.Inside == false && BlackController.Down.Inside == true) {
			BlackWhiteDown = BlackController.Down.myBody.position.y;
		}
		if (WhiteController.Up.Inside == false && BlackController.Up.Inside == true) {
			BlackWhiteUp = BlackController.Up.myBody.position.y;
		}
		if (WhiteController.Right.Inside == false && BlackController.Right.Inside == true) {
			BlackWhiteRight = BlackController.Right.myBody.position.x;
		}
		if (WhiteController.Left.Inside == false && BlackController.Left.Inside == true) {
			BlackWhiteLeft = BlackController.Left.myBody.position.x;
		}
		#endregion

		#region Black & White Controller
		if (WhiteController.Down.Inside == true && BlackController.Down.Inside == true) {
			if (WhiteController.Down.myBody.position.y > BlackController.Down.myBody.position.y) {
				BlackWhiteDown = WhiteController.Down.myBody.position.y;
			} else {
				BlackWhiteDown = BlackController.Down.myBody.position.y;
			}
		}
		if (WhiteController.Up.Inside == true && BlackController.Up.Inside == true) {
			if (WhiteController.Up.myBody.position.y < BlackController.Up.myBody.position.y) {
				BlackWhiteUp = WhiteController.Up.myBody.position.y;
			} else {
				BlackWhiteUp = BlackController.Up.myBody.position.y;
			}
		}
		if (WhiteController.Right.Inside == true && BlackController.Right.Inside == true) {
			if (WhiteController.Right.myBody.position.x < BlackController.Right.myBody.position.x) {
				BlackWhiteRight = WhiteController.Right.myBody.position.x;
			} else {
				BlackWhiteRight = BlackController.Right.myBody.position.x;
			}
		}
		if (WhiteController.Left.Inside == true && BlackController.Left.Inside == true) {
			if (WhiteController.Left.myBody.position.x > BlackController.Left.myBody.position.x) {
				BlackWhiteLeft = WhiteController.Left.myBody.position.x;
			} else {
				BlackWhiteLeft = BlackController.Left.myBody.position.x;
			}
		}
		#endregion
		if (WhiteController.Down.Inside == false && BlackController.Down.Inside == false) {
			BlackWhiteLeft = 0;
		}
		if (WhiteController.Up.Inside == false && BlackController.Up.Inside == false) {
			BlackWhiteUp = 0;
		}
		if (WhiteController.Right.Inside == false && BlackController.Right.Inside == false) {
			BlackWhiteRight = 0;
		}
		if (WhiteController.Left.Inside == false && BlackController.Left.Inside == false) {
			BlackWhiteDown = 0;
		}

		middle = new Vector2 ((BlackWhiteRight + BlackWhiteLeft) / 2, (BlackWhiteUp + BlackWhiteDown) / 2);
		#region adjust localScale
		prefabX = Mathf.Abs ((BlackWhiteLeft - middle.x) * 10);
		prefabY = Mathf.Abs ((BlackWhiteUp - middle.y) * 10);
		#endregion

	}

	void LateUpdate () {
		if (WhiteController == null || BlackController == null || BlackWhiteLeft == 0 ||
			BlackWhiteUp == 0 || BlackWhiteRight == 0 || BlackWhiteDown == 0) {
			Destroy (storeSquare);
			return;
		}
		if (storeData != middle && middle != Vector2.zero) {
			Destroy (storeSquare);
			storeData = middle;

			GameObject OverlapSquare = (GameObject) Instantiate (Prefab, storeData, Quaternion.identity);
			OverlapSquare.name = "OverlapSquare";
			OverlapSquare.transform.localScale = new Vector2 (prefabX, prefabY);
			storeSquare = OverlapSquare;
		} else if (middle == Vector2.zero) {
			Destroy (storeSquare);
		}
	}
}