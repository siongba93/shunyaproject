﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class MovementExtension
{
	public static bool complete = false;

	public static void moveTo (this Transform body, Vector3 Des, float speed)
	{
		Transform myBody = body.transform;
		myBody.position = Vector3.MoveTowards (myBody.position, Des, speed);
	}

	public static IEnumerator moveTo (this Transform body, Vector3 Des, float speed, Action onComplete)
	{
		if (Vector3.Distance (body.transform.position, Des) < 0.01f || complete) {
			onComplete ();
			complete = true;
		} else if (complete == false) {
			body.moveTo (Des, speed);
			yield return null;
		}
	}

}
