﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SquareBoxState {
	HIDING,
	White,
	Black,
}

public enum SquareType {
	SquareWhite,
	SquareBlack,
	SquareBlackWhite
}

public enum State {
	Normal,
	platformMoving,
	rotate
}

public enum SwitchState {
	Red,
	Blue
}

public enum SquareControllerType {
	SquareWaterController,
	SquareWindController,
	SquareFireController,
	SquareEarthController
}

public enum CameraBlockState {
	HORIZONTAL,
	VERTICAL,
	ALL
}

public enum DirectionState {
	Left,
	Right,
}

public enum ButtonSquareState {
	Normal,
	Highlight,
	Clicked
}

public enum LighterState {
	ON,
	OFF
}

public enum PrefabType {
	// GroundSquare,
	// ButtonSquare,
	// BoxSquare,
	// BlackWhiteSquare,
	// WalkingSquare,
	// CameraSquare,
	// ColorSquare,
	// TeleportSquare,
	// TurnHorizontalSquare,
	EarthSquare,
	WaterSquare,
	FireSquare,
	WindSquare
}

public enum TeleportState {
	Left,
	Right,
	Up,
	Down,
	Null
}

public enum TeleportType {
	leftRight,
	upDown,
}

public enum BlackWhiteState {
	colorChanging,
	imageChanging,
	physicsCollisionIgnore
}

public enum ColorSquareState {
	RED,
	ORANGE,
	YELLOW,
	GREEN,
	BLUE,
	PINK,
	PURPLE,
	EMPTY,
}

namespace DataBase {
	public class Model : MonoBehaviour {
		public static string SquareGround = "SquareGround";
		public static string SquareBox = "SquareBox";
		public static string SquareButton = "SquareButton";
		public static string SquareBlackWhite = "SquareBlackWhite";
		public static string SquareBlack = "SquareBlack";
		public static string SquareWhite = "SquareWhite";
		public static string SquareWalking = "SquareWalking";
		public static string SquareWalkingChild = "SquareWalkingChild";
		public static string SquareColor = "SquareColor";
		public static string SquareCamera = "SquareCamera";
		public static string SquareCameraPlane = "SquareCameraPlane";
		public static string SquareFire = "SquareFire";
		public static string SquareWater = "SquareWater";
		public static string SquareWind = "SquareWind";
		public static string SquareEarth = "SquareEarth";
		public static string Ground = "ground";
		public static string Player = "Player";
		public static string PlayerCheckLeft = "PlayerCheckLeft";
		public static string PlayerCheckDown = "PlayerCheckDown";
		public static string PlayerStateCheck = "PlayerStateCheck";
		public static string Mouse = "Mouse";
		public static string Ladder = "Ladder";
		public static string LadderTop = "LadderTop";
		public static string LadderBottom = "LadderBottom";
		public static string Enemy = "Enemy";
		public static string EnemyUp = "EnemyUp";
		public static string JumpGround = "JumpGround";
		public static string Trap = "Trap";
		public static string Key = "key";
		public static string KeyDoor = "KeyDoor";
		public static string Bubble = "Bubble";
		public static string EarthSquareRegion = "EarthSquareRegion";
		public static string WaterSquareRegion = "WaterSquareRegion";
		public static string WindSquareRegion = "WindSquareRegion";
		public static string FireSquareRegion = "FireSquareRegion";

		public static string SquareHolder = "SquareHolder";
		public static RenderTexture cameraSquareTexture;
	}

	public class LayerNum : MonoBehaviour {
		public static int LayerPlayer = 12;
		public static int Layer01 = 29;
		public static int Layer02 = 30;
		public static int Layer03 = 31;
		public static int DEFAULT = 0;
		public static int TRANSPARENT_FX = 1;
		public static int IGNORE_RAYCAST = 2;
		public static int WATER = 3;
		public static int UI = 4;
		public static int GROUND = 8;
		public static int DRAW = 9;
		public static int MOUSE = 10;
		public static int CHECK_GROUND = 11;
		public static int PLAYER = 12;
		public static int SQUARE_BLACK_WHITE = 13;
		public static int NORMAL_OBJECT = 14;
		public static int INTERACTIVE_OBJECT = 15;
		public static int ENEMY_OBJECT = 16;
		public static int BACKGROUND_OBJECT = 17;
		public static int CHECK_FRONT = 18;
		public static int INVISIBLE_OBJECT = 19;
	}

	public class SquareData : MonoBehaviour {
		public static float windHorizontalForce = 0;
		public static float windVerticalForce = 4;
		public static int currentSquare = 0;
		public static bool canDraw = false;

	}
}