﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquarePrefabStoreController : MonoBehaviour {

	public GameObject GroundSquare;
	public GameObject BlackSquare;
	public GameObject WhiteSquare;
	public GameObject OverlapSquare;
	public GameObject BoxSquare;
	public GameObject ButtonSquare;
	public GameObject WalkingSquare;
	public GameObject CameraSquare;
	public GameObject CameraPlaneSquare;
	public GameObject ColorSquare;
	public GameObject TeleportSquare;
	public GameObject TurnHorizontalSquare;
	public GameObject EarthSquare;
	public GameObject WaterSquare;
	public GameObject FireSquare;
	public GameObject WindSquare;
}