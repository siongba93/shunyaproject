﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovementCallback : MonoBehaviour
{
	public Vector3 Destination;
	public Vector3 force;
	public Transform body;
	public bool canMove = false;

	void Update ()
	{
		if (canMove) {
			
		}
	}

	void OnGUI ()
	{
		if (GUI.Button (new Rect (5, 5, 200, 40), "PlayMovement")) {
			StartCoroutine (
				body.moveTo (Destination, 1 * Time.deltaTime, () => {
					Debug.Log ("Movement Done");
				})
			);
		}
	}
}
