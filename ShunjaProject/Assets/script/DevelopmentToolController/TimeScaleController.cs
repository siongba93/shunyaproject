﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScaleController : MonoBehaviour {

	public float time;

	// Update is called once per frame
	void Update () {
		Time.timeScale = time;
	}
	public void resumeTime () {
		Time.timeScale = 1;
	}
}