﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectionController : MonoBehaviour
{
    public static int Level;

    public RectTransform selectionPosition;
    public AudioSource audioSelectionMovement;

    public Vector2 position;

    // Image
    public ImageScreenController ImageScreen;

    public AudioClip audioStart;
    public AudioClip audioSelection;

    // Position
    private Vector2 upPosition = new Vector2(18, 174);
    private Vector2 rightPosition = new Vector2(184, 8);
    private Vector2 downPosition = new Vector2(11, -165);
    private Vector2 leftPosition = new Vector2(-158, 8);
    public int currentLevel = 0;

    public float moveSpeed = 0;

    void Update()
    {
        _selectionInput();
        _selectionMovement();
        _enterLevel();
    }

    private void _selectionInput()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            AudioManager.playAudioClip(audioSelectionMovement, audioSelection, 0.2f);
            currentLevel = 0;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            AudioManager.playAudioClip(audioSelectionMovement, audioSelection, 0.2f);
            currentLevel = 1;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            AudioManager.playAudioClip(audioSelectionMovement, audioSelection, 0.2f);
            currentLevel = 2;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            AudioManager.playAudioClip(audioSelectionMovement, audioSelection, 0.2f);
            currentLevel = 3;
        }
        position = selectionPosition.localPosition;
        Level = currentLevel;
    }

    private void _selectionMovement()
    {

        Vector2 destination = Vector2.zero;
        switch (currentLevel)
        {
            case 0:
                destination = upPosition;
                break;
            case 1:
                destination = rightPosition;
                break;
            case 2:
                destination = downPosition;
                break;
            case 3:
                destination = leftPosition;
                break;
        }
        selectionPosition.localPosition = Vector2.MoveTowards(selectionPosition.localPosition, destination, moveSpeed * Time.deltaTime);
    }

    private void _enterLevel()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            AudioManager.playAudioClip(audioSelectionMovement, audioStart);
            ImageScreen.uiImageFadeIn();
            StartCoroutine(enterLevel(2));
        }
    }

    public IEnumerator enterLevel(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        switch (SelectionController.Level)
        {
            case 0:
                SceneManager.LoadScene("EarthScreen");
                break;
            case 1:
                SceneManager.LoadScene("WaterScreen");
                break;
            case 2:
                SceneManager.LoadScene("WindScreen");
                break;
            case 3:
                SceneManager.LoadScene("FireScreen");
                break;
        }
    }



}
