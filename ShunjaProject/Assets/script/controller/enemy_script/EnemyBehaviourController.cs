﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataBase;
public class EnemyBehaviourController : MonoBehaviour {

	void OnCollisionEnter2D(Collision2D obj){
		if (obj.gameObject.tag == Model.SquareGround) {
			this.transform.parent = obj.transform;
		}
	}
}
