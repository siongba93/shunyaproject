﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemySquareState {
	UP,
	LEFT,
	DOWN,
	RIGHT
}

public class EnemyMoveSquareController : MonoBehaviour {

	public Animator anim;

	public EnemySquareState currentState = EnemySquareState.UP;
	public Transform groundPrefab;
	public Transform spriteTransform;
	public float speed;
	public Transform enemyUp;
	public SpriteRenderer image;
	public SpriteRenderer crystal;

	public EnemySquareCheckingInsideController controller;

	public Vector2 position01;
	public Vector2 position02;
	public Vector2 position03;
	public Vector2 position04;

	public void setMovementSpeed (float value) {
		speed = value;
	}

	public void setSpriteColor (Color newColor) {
		image.color = newColor;
	}

	public void setCrystalColor (Color newColor) {
		crystal.color = newColor;
	}

	public void setAnimatorEnabled (bool state) {
		anim.enabled = state;
	}

	// Use this for initialization
	void FixedUpdate () {
		controller.checking ();
		_platformSquareMove ();
		_checkDestination ();
	}

	private void _platformSquareMove () {
		if (currentState == EnemySquareState.UP) {
			Quaternion target = Quaternion.Euler (0, 0, 0);
			spriteTransform.transform.rotation = Quaternion.Slerp (spriteTransform.transform.rotation, target, Time.deltaTime * speed);
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, position02, speed * Time.deltaTime);
		} else if (currentState == EnemySquareState.LEFT) {
			Quaternion target = Quaternion.Euler (0, 0, -90);
			spriteTransform.transform.rotation = Quaternion.Slerp (spriteTransform.transform.rotation, target, Time.deltaTime * speed);
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, position03, speed * Time.deltaTime);
		} else if (currentState == EnemySquareState.DOWN) {
			Quaternion target = Quaternion.Euler (0, 0, -180);
			spriteTransform.transform.rotation = Quaternion.Slerp (spriteTransform.transform.rotation, target, Time.deltaTime * speed);
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, position04, speed * Time.deltaTime);
		} else if (currentState == EnemySquareState.RIGHT) {
			Quaternion target = Quaternion.Euler (0, 0, -270);
			spriteTransform.transform.rotation = Quaternion.Slerp (spriteTransform.transform.rotation, target, Time.deltaTime * speed);
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, position01, speed * Time.deltaTime);
		}
	}

	private void _checkDestination () {
		if (Vector2.Distance (groundPrefab.position, position02) < 0.2) {
			currentState = EnemySquareState.LEFT;
		} else if (Vector2.Distance (groundPrefab.position, position03) < 0.2) {
			currentState = EnemySquareState.DOWN;
		} else if (Vector2.Distance (groundPrefab.position, position04) < 0.2) {
			currentState = EnemySquareState.RIGHT;
		} else if (Vector2.Distance (groundPrefab.position, position01) < 0.2) {
			currentState = EnemySquareState.UP;
		}
	}

}