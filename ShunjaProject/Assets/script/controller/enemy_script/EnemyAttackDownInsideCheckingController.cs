﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttackDownInsideCheckingController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;
	public EnemyFlyAttackDownController controller;

	public void checking () {
		_insideChecking ();
		_checkState ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
	}

	private void _checkState () {
		if (WhiteInside == false) {
			controller.setAttackSpeed (0);
			controller.setBackSpeed (0);
			controller.setCrystalColor (Color.grey);
			return;
		}

		if (WhiteInside == true) {
			controller.setAttackSpeed (10);
			controller.setBackSpeed (3);
			controller.setCrystalColor (Color.green);
		}

	}
}