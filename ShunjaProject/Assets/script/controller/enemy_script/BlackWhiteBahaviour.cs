﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class BlackWhiteBahaviour : MonoBehaviour {

	[Header ("ImageFeature")]
	public Transform ImagePos;
	public SpriteRenderer Image;
	public Sprite NormalImage;
	public Sprite WhiteImage;
	public Sprite BlackImage;
	public Sprite OverlapImage;

	[Header ("ColorFeature")]
	public Color NormalColor;
	public Color WhiteColor;
	public Color BlackColor;
	public Color OverlapColor;

	[Header ("GeneralComponent")]
	public BlackWhiteState CurrState;
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	void Update () {
		_insideChecking ();
		_triggerFeature ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside && RightController.WhiteInside &&
			UpController.WhiteInside && DownController.WhiteInside) {
			WhiteInside = true;
		} else {
			WhiteInside = false;
		}
		if (LeftController.BlackInside && RightController.BlackInside &&
			UpController.BlackInside && DownController.BlackInside) {
			BlackInside = true;
		} else {
			BlackInside = false;
		}
		if (WhiteInside == false && BlackInside == false) {
			ColorChanging (NormalColor);
			ImageChanging (NormalImage);
		} else if (WhiteInside && BlackInside) {
			switch (CurrState) {
				case BlackWhiteState.colorChanging:
					ColorChanging (OverlapColor);
					break;
				case BlackWhiteState.imageChanging:
					ImageChanging (OverlapImage);
					break;
				case BlackWhiteState.physicsCollisionIgnore:
					// Physics2D.IgnoreLayerCollision (12, 32, bool ignore);
					break;
			}
		}
	}

	private void _triggerFeature () {
		if (WhiteInside && BlackInside == false) {
			switch (CurrState) {
				case BlackWhiteState.colorChanging:
					ColorChanging (WhiteColor);
					break;
				case BlackWhiteState.imageChanging:
					ImageChanging (WhiteImage);
					break;
				case BlackWhiteState.physicsCollisionIgnore:
					// Physics2D.IgnoreCollision (myCollision, playerGameObject.GetComponent<BoxCollider2D> ());
					// Physics2D.IgnoreCollision (myCollision, checkGroundGameObject.GetComponent<BoxCollider2D> ());
					break;
			}
		} else if (BlackInside && WhiteInside == false) {
			switch (CurrState) {
				case BlackWhiteState.colorChanging:
					ColorChanging (BlackColor);
					break;
				case BlackWhiteState.imageChanging:
					ImageChanging (BlackImage);
					break;
				case BlackWhiteState.physicsCollisionIgnore:
					// Physics2D.IgnoreCollision (myCollision, playerGameObject.GetComponent<BoxCollider2D> ());
					// Physics2D.IgnoreCollision (myCollision, checkGroundGameObject.GetComponent<BoxCollider2D> ());
					break;
			}
		}
	}

	private void ColorChanging (Color color) {
		Image.color = color;
	}

	private void ImageChanging (Sprite image) {
		Image.sprite = image;
	}
}