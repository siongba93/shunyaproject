﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class EnemyChasingPlayerGround : MonoBehaviour {
	public float speedAttack;
	public float speedBack;

	public float direction;
	public float originalDirection;
	public float backDirection;

	public bool playerInside = false;
	public Vector3 originalPos;
	public Vector3 destinationPos;
	public SpriteRenderer crystal;
	public Transform[] enemySprite;
	public EnemyAttackHorizontalCheckingInsideController controller;

	public void setAttackSpeed (float newSpeed) {
		speedAttack = newSpeed;
	}

	public void setBackSpeed (float newSpeed) {
		speedBack = newSpeed;
	}

	public void setCrystalColor (Color color) {
		crystal.color = color;
	}

	void FixedUpdate () {
		_attackMovement ();
		controller.checking ();
	}

	public void setDirection () {
		enemySprite[0].rotation = Quaternion.Euler (0, direction, 0);
		enemySprite[1].rotation = Quaternion.Euler (0, direction, 0);
	}

	private void _attackMovement () {
		if (playerInside == false) {
			direction = backDirection;
			this.transform.position = Vector3.MoveTowards (this.transform.position, originalPos, speedBack * Time.deltaTime);
		} else {
			direction = originalDirection;
			this.transform.position = Vector3.MoveTowards (this.transform.position, destinationPos, speedAttack * Time.deltaTime);
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = false;
		}
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = false;
		}
	}
}