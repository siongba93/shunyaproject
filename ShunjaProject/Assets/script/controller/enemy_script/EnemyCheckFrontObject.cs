﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using NormalEnemyData;
using UnityEngine;
public class EnemyCheckFrontObject : MonoBehaviour {
	public Collider2D other;

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.KeyDoor ||
			collision.gameObject.tag == Model.SquareEarth) {
			EnemyNormalState.frontBlock = true;
		}
	}

}