﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using NormalEnemyData;
using UnityEngine;
public class EnemyCheckGround : MonoBehaviour {
	public Collider2D other;

	void Update () {
		if (other == null) {
			EnemyNormalState.isGround = false;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareEarth) {
			EnemyNormalState.isGround = true;
			other = collision;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareEarth) {
			EnemyNormalState.isGround = false;
			other = null;
		}
	}

}