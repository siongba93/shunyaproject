﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStateController : MonoBehaviour {

	public int healPoint = 3;
	public int Direction = 1;

	public GameObject key;
	public bool alive = true;

	public KeyHolderController controller;
	public Text label;

	void Start () {
		alive = true;
	}

	void Update () {
		label.text = "" + healPoint;
		if (healPoint == 0) {
			alive = false;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == Model.PlayerCheckDown) {
			healPoint -= 1;
		}
	}
}