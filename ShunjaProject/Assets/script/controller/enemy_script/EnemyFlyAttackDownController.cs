﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class EnemyFlyAttackDownController : MonoBehaviour {
	public float speedAttack;
	public float speedBack;

	public bool playerInside = false;
	public Vector3 originalPos;
	public Vector3 destinationPos;
	public SpriteRenderer crystal;
	public EnemyAttackDownInsideCheckingController controller;

	public void setAttackSpeed (float newSpeed) {
		speedAttack = newSpeed;
	}

	public void setBackSpeed (float newSpeed) {
		speedBack = newSpeed;
	}

	public void setCrystalColor (Color color) {
		crystal.color = color;
	}

	void FixedUpdate () {
		_attackMovement ();
		controller.checking ();
	}

	private void _attackMovement () {
		if (playerInside == false) {
			this.transform.position = Vector3.MoveTowards (this.transform.position, originalPos, speedBack * Time.deltaTime);
		} else {
			this.transform.position = Vector3.MoveTowards (this.transform.position, destinationPos, speedAttack * Time.deltaTime);
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = false;
		}
	}
}