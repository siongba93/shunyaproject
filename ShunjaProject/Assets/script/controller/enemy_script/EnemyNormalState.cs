﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NormalEnemyData {
	public class EnemyNormalState : MonoBehaviour {
		public static bool frontCanMove = false;
		public static bool frontBlock = false;
		public static bool isGround = false;

		public static int enemyDirection = 1;
	}
}