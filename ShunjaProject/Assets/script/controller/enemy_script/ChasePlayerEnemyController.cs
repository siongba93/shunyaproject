﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayerEnemyController : MonoBehaviour {

	public Transform myTransform;
	public GameObject cloud;
	public PolygonCollider2D collision;
	public SpriteRenderer crystal;
	public float moveSpeed;
	public Transform target;
	public Animator anim;

	public ChasePlayerInsideCheckingController controller;

	public void setMoveSpeed (float speed) {
		moveSpeed = speed;
	}

	public void setCrystalColor (Color color) {
		crystal.color = color;
	}
	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player").GetComponent<Transform> ();
	}

	// Update is called once per frame
	void Update () {
		controller.checking ();
		myTransform.position = Vector3.MoveTowards (myTransform.position, target.position, moveSpeed * Time.deltaTime);
	}

	void OnCollisionEnter2D (Collision2D collider) {
		cloud.SetActive (false);
		anim.enabled = true;
		collision.enabled = false;
		crystal.enabled = false;
		setMoveSpeed (0);
		anim.SetFloat ("speed", 1);
	}

	public void destroyMethod () {
		Destroy (this.gameObject);
	}
}