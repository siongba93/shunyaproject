﻿using System.Collections;
using System.Collections.Generic;
using NormalEnemyData;
using UnityEngine;

public class EnemyNormalController : MonoBehaviour {
	public EnemyStateController enemyState;

	public Rigidbody2D enemyBody;
	public Transform enemyTransform;
	public BoxCollider2D enemyBodyCollision;
	public BoxCollider2D enemyCheckUpCollision;
	public BoxCollider2D enemyCheckDownCollision;
	public BoxCollider2D enemyCheckFrontCollision;

	public EnemyCheckGround checkGround;
	public EnemyCheckFrontGround checkFrontGround;
	public EnemyCheckFrontObject checkFrontObject;
	public Animator anim;

	public float speed;

	public void setGravity (float value) {
		enemyBody.gravityScale = value;
	}

	public void setMoveSpeed (float value) {
		speed = value;
	}

	public void setBoxCollision (bool state) {
		enemyBodyCollision.enabled = state;
		enemyCheckUpCollision.enabled = state;
		enemyCheckDownCollision.enabled = state;
		enemyCheckFrontCollision.enabled = state;
	}

	void Update () {
		if (enemyState.alive == false) {
			_enemyDie ();
			return;
		}
		_moving ();
		_onGroundController ();
		_changeDirection ();
		_setEnemyDirection ();
	}

	private void _enemyDie () {
		setBoxCollision (false);
		enemyTransform.localRotation = Quaternion.Euler (0, 0, 180);
	}

	private void _moving () {
		if (EnemyNormalState.isGround) {
			enemyTransform.Translate (Vector2.right * speed * Time.deltaTime);
		}
	}

	private void _onGroundController () {
		anim.SetBool ("isGround", EnemyNormalState.isGround);
	}

	private void _changeDirection () {
		if (EnemyNormalState.frontCanMove == false) {
			switch (EnemyNormalState.enemyDirection) {
				case 1:
					EnemyNormalState.enemyDirection = -1;
					EnemyNormalState.frontCanMove = true;
					break;
				case -1:
					EnemyNormalState.enemyDirection = 1;
					EnemyNormalState.frontCanMove = true;
					break;
			}
		}
		if (EnemyNormalState.frontBlock == true) {
			switch (EnemyNormalState.enemyDirection) {
				case 1:
					EnemyNormalState.enemyDirection = -1;
					EnemyNormalState.frontBlock = false;
					break;
				case -1:
					EnemyNormalState.enemyDirection = 1;
					EnemyNormalState.frontBlock = false;
					break;
			}
		}
	}

	private void _setEnemyDirection () {
		Transform _enemyTransform = enemyTransform.transform;
		if (EnemyNormalState.enemyDirection == 1) {
			_enemyTransform.localRotation = Quaternion.Euler (0, 0, 0);
		} else if (EnemyNormalState.enemyDirection == -1) {
			_enemyTransform.localRotation = Quaternion.Euler (0, -180, 0);
		}
	}
}