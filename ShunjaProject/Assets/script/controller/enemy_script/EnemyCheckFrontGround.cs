﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using NormalEnemyData;
using UnityEngine;

public class EnemyCheckFrontGround : MonoBehaviour {

	public LayerMask Ground;
	public SpriteRenderer enemySprite;
	public Transform myTrans;
	float myWidth, myHeight;
	public float Length, height = 0.5f;
	public Vector2 lineCastPos;

	void Start () {
		myWidth = enemySprite.bounds.extents.x;
		myHeight = enemySprite.bounds.extents.y / 2;
	}

	void Update () {
		lineCastPos = myTrans.position.toVector2 () + myTrans.right.toVector2 () * myWidth - Vector2.up * myHeight * height;
		Debug.DrawLine (lineCastPos, lineCastPos - Vector2.up * myHeight * height, Color.red);
		if (EnemyNormalState.isGround) {
			EnemyNormalState.frontCanMove = Physics2D.Linecast (lineCastPos, lineCastPos - Vector2.up * myHeight * height, Ground);
		}
	}

}