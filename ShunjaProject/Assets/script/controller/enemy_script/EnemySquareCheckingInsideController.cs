﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySquareCheckingInsideController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public EnemyMoveSquareController controller;

	public bool active = true;

	public void setActive (bool state) {
		active = state;
	}

	public void checking () {
		_insideChecking ();
		if (active) {
			_checkState ();
		}
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
	}

	private void _checkState () {
		if (WhiteInside == false) {
			controller.setMovementSpeed (0);
			controller.setCrystalColor (Color.grey);
			controller.setSpriteColor (Color.grey);
			controller.setAnimatorEnabled (false);
			return;
		}

		if (WhiteInside == true) {
			controller.setMovementSpeed (3);
			controller.setCrystalColor (Color.green);
			controller.setSpriteColor (Color.white);
			controller.setAnimatorEnabled (true);
		}
	}
}