﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum CatState
{
    IDLE,
    DRAWING
}
public class CatController : MonoBehaviour
{
    public SpriteRenderer catSprite;
    public Sprite idleSprite;
    public Sprite drawSprite;

    public Transform catTransform;

    private CatState _catState;

    public void setIdle()
    {
        _catState = CatState.IDLE;
        catSprite.sprite = idleSprite;
    }

    public void setDrawing()
    {
        _catState = CatState.DRAWING;
        catSprite.sprite = drawSprite;
    }

    private void Update()
    {
        if (_catState == CatState.DRAWING)
        {
            checkMouseMovement();
        }
    }

    private void checkMouseMovement()
    {
        if (Input.GetAxis("Mouse X") < 0)
        {
            //Code for action on mouse moving left
            print("Mouse moved left");
            catTransform.localRotation = Quaternion.Euler(0, -180, 0);
        }
        if (Input.GetAxis("Mouse X") > 0)
        {
            //Code for action on mouse moving right
            print("Mouse moved right");
            catTransform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }


}
