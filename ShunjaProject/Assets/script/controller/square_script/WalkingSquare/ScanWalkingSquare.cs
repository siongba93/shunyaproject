﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanWalkingSquare : MonoBehaviour
{
	public SpriteRenderer sprite;
	public BoxCollider2D collisionBox;
	public BoxCollider2D ground;
	public Transform myBody;
	private Vector3 worldPos;
	public string TargetName;
	public bool Inside;

	void Update ()
	{
	}

	void OnTriggerStay2D (Collider2D collision)
	{
		if (collision.gameObject.name == "" + TargetName && collision.gameObject.tag == "SquareWalkingChild") {
			sprite.color = Color.red;
			ground.enabled = false;
			ground.gameObject.SetActive (false);
			Inside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision)
	{
		if (collision.gameObject.name == "" + TargetName && collision.gameObject.tag == "SquareWalkingChild") {
			sprite.color = Color.white;
			ground.enabled = true;
			ground.gameObject.SetActive (true);
			Inside = false;
			Debug.Log ("Exit");
		}
	}
		

}
