﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class WalkingStateController : MonoBehaviour {

	public GameObject square;
	public string state;
	public PlayerSquareStatusController SquareState;

	void Update () {
		if (SquareState.stillInside) {
			square = SquareState.other.gameObject;
			if (square.tag == Model.SquareWalking) {
				state = "Inside";
			}
		} else {
			square = null;
			state = "Outside";
		}
	}
}