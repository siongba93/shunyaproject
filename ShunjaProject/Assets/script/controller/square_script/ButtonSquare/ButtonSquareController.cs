﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataBase;

public class ButtonSquareController : MonoBehaviour {
	public SpriteRenderer Image;
	public Color NormalColor;
	public Color HighlightColor;
	public Color ClickedColor;
	public string InterativeObjectName= "";
	public ButtonSquareState CurrState;

	void Update(){
		if (CurrState == ButtonSquareState.Highlight) {
			if (Input.GetMouseButton (0)) {
				Image.color = ClickedColor;
			} else {
				Image.color = HighlightColor;
			}
		} 
	}

	void OnTriggerStay2D(Collider2D collision){
		if (collision.gameObject.tag == InterativeObjectName) {
			Image.color = HighlightColor;
			CurrState = ButtonSquareState.Highlight;
		}
	}

	void OnTriggerExit2D(Collider2D collision){
		if (collision.gameObject.tag == InterativeObjectName) {
			Image.color = NormalColor;
			CurrState = ButtonSquareState.Normal;
		}
	}
}
