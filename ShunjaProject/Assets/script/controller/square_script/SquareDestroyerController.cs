﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareDestroyerController : MonoBehaviour {

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "SquareWhite" ||
			collision.gameObject.tag == "SquareBlack") {
			collision.gameObject.GetComponent<BlackWhiteSquareController> ().destroy ();
			// collision.gameObject.destroy ();
		}
	}

}