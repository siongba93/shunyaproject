﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityCursorControl;
using UnityEngine;

public class TeleportHolderController : MonoBehaviour {

	public GameObject outerLeft;
	public GameObject outerRight;
	public GameObject interLeft;
	public GameObject interRight;

	public TeleportState teleportState;
	public GameObject Player;

	public CameraController camController;
	public DrawSquareController drawController;
	public PlayerMovementController playerController;
	public TeleportPlayerController teleportPlayerController;

	public TeleportController controllerOuterLeft;
	public TeleportController controllerOuterRight;
	public TeleportController controllerInterLeft;
	public TeleportController controllerInterRight;

	public bool outerLeftInside = false;
	public bool outerRightInside = false;
	public bool interLeftInside = false;
	public bool interRightInside = false;

	public bool insideHolder = false;
	public bool fakePlayerLeftInsideHolder = false;
	public bool fakePlayerRightInsideHolder = false;
	public int transferNum = 0;

	void Start () {
		transferNum = 0;
		Player = GameObject.FindWithTag ("Player");
		playerController = Player.GetComponent<PlayerMovementController> ();
		camController = GameObject.FindWithTag ("MainCamera").GetComponent<CameraController> ();
		drawController = GameObject.FindWithTag ("DrawSquareController").GetComponent<DrawSquareController> ();
		teleportPlayerController = GameObject.FindWithTag ("TeleportPlayerController").GetComponent<TeleportPlayerController> ();

		controllerOuterLeft.setTeleportSquare (this.transform);
		controllerOuterRight.setTeleportSquare (this.transform);
		controllerInterLeft.setTeleportSquare (this.transform);
		controllerInterRight.setTeleportSquare (this.transform);

	}

	// Update is called once per frame
	void Update () {
		if (insideHolder == false) {
			interLeft.SetActive (false);
			interRight.SetActive (false);
			outerLeft.SetActive (true);
			outerRight.SetActive (true);
		} else {
			interLeft.SetActive (true);
			interRight.SetActive (true);
			outerLeft.SetActive (false);
			outerRight.SetActive (false);
		}

		if (controllerOuterLeft.playerInside == true) {
			playerController.PlayerSprite.sortingLayerName = "player_fake_2";
			outerLeftInside = true;
			teleportState = TeleportState.Left;
		} else if (controllerOuterRight.playerInside == true) {
			playerController.PlayerSprite.sortingLayerName = "player_fake_2";
			outerRightInside = true;
			teleportState = TeleportState.Right;
		} else if (controllerInterLeft.playerInside == true) {
			playerController.PlayerSprite.sortingLayerName = "player_fake_2";
			interLeftInside = true;
			teleportState = TeleportState.Left;
		} else if (controllerInterRight.playerInside == true) {
			playerController.PlayerSprite.sortingLayerName = "player_fake_2";
			interRightInside = true;
			teleportState = TeleportState.Right;
		} else {
			playerController.PlayerSprite.sortingLayerName = "player";
			teleportState = TeleportState.Null;
			switch (insideHolder) {
				case true:
					teleportPlayerController.updateFakePlayerInsidePosition ();
					break;
				case false:
					teleportPlayerController.updateFakePlayerOutsidePosition ();
					break;
			}
		}
		teleportPlayerController.updatePlayerPosition ();
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			insideHolder = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			insideHolder = false;
		}
	}
}