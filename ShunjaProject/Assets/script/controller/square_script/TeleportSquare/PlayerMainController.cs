﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMainController : PlayerStateController {

	public Transform mainPlayer;

	public GameObject spritePlayerObject;
	public GameObject collisionBoxObject;
	public GameObject dieEffectObject;
	public dieController dieMainController;
	public PlayerMovementController moveController;
	public KeyHolderController keyHolderController;
	public BoxCollider2D checkGround;
	public BoxCollider2D checkRight;
	public BoxCollider2D checkUp;

	public GameObject trail;

	public bool playerDieState = false;
	public Vector2 resetPosition;
	public GameObject key;
	// Update is called once per frame

	public SpriteRenderer player;

	public Animator playerAnim;

	public Rigidbody2D playerBody;

	public void Update () { }

	public void setResetPosition (Vector2 newPosition) {
		resetPosition = newPosition;
	}

	public void setPlayerColor (Color newColor) {
		player.color = newColor;
	}

	public void setKinematicType () {
		playerBody.velocity = Vector2.zero;
		playerBody.bodyType = RigidbodyType2D.Kinematic;
		moveController.enabled = false;
	}

	public void setDynamicType () {
		playerBody.bodyType = RigidbodyType2D.Dynamic;
		moveController.enabled = true;
	}

	public void setAnimationState (bool state) {
		playerAnim.enabled = state;
	}

	public void reset () {
		playerDieState = false;
		PlayerIsGround = false;
		PlayerCanMoveLeft = true;
		PlayerCanMoveRight = true;
		HorizontalForceRight = InitialHorizontalForce;
		HorizontalForceLeft = -InitialHorizontalForce;
		spritePlayerObject.SetActive (true);
		collisionBoxObject.SetActive (true);
		moveController.enabled = true;
		dieEffectObject.SetActive (false);
		moveController.resetRigidbody ();
	}

	public void playerDie () {
		if (playerDieState == false) {
			if (PlayerHoldingKey == true) {
				PlayerHoldingKey = false;
				keyHolderController.setKeyEnabled (false);
				Instantiate (key, this.transform.position, Quaternion.identity);
			}
			PlayerStatus = PlayerCondition.Normal;
			collisionBoxObject.SetActive (false);
			moveController.fixedRigidbody ();
			moveController.enabled = false;
			spritePlayerObject.SetActive (false);
			dieEffectObject.SetActive (true);
			dieMainController.playDie ();
			WaterHealPoint = InitialWaterHealPoint;
			FireHealPoint = InitialFireHealPoint;
			playerDieState = true;
		}
	}

	public void stopAllAction () {
		trail.SetActive (true);
		PlayerStatus = PlayerCondition.Normal;
		collisionBoxObject.SetActive (false);
		spritePlayerObject.SetActive (false);
		moveController.fixedRigidbody ();
		moveController.enabled = false;
	}

	public void resetAllAction () {
		trail.SetActive (false);
		PlayerStatus = PlayerCondition.Normal;
		collisionBoxObject.SetActive (true);
		spritePlayerObject.SetActive (true);
		moveController.resetRigidbody ();
		moveController.enabled = true;
	}

	public void resetPlayerPosition () {
		mainPlayer.position = resetPosition;
	}

	public void resetWaterHealPoint () {
		WaterHealPoint = InitialWaterHealPoint;
	}

	public void resetFireHealPoint () {
		FireHealPoint = InitialFireHealPoint;
	}
}