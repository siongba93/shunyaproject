﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlayerMovementController : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// //set player horizontal movement control
	// private void playerHorizontalMove () {
	// 	float horizontalInput = Input.GetAxisRaw ("Horizontal");
	// 	Transform playerTransform = Player.transform;
	// 	if (horizontalInput != 0 && PlayerSquating == false) {
	// 		if (PlayerCanMove) {
	// 			PlayerMoving = true;
	// 		} else {
	// 			PlayerMoving = false;
	// 		}
	// 		jump ();
	// 		if (horizontalInput > 0) {
	// 			PlayerDirection = 1;
	// 			playerTransform.Translate (SlopeWalk * HorizontalForce * Time.deltaTime);
	// 			return;
	// 		} else if (horizontalInput < 0) {
	// 			PlayerDirection = -1;
	// 			playerTransform.Translate (SlopeWalk * HorizontalForce * Time.deltaTime);
	// 			return;
	// 		}
	// 	} else {
	// 		PlayerMoving = false;
	// 	}
	// 	jump ();
	// }

	// //set player jump control
	// private void jump () {
	// 	Rigidbody2D playerBody = Player.GetComponent<Rigidbody2D> ();

	// 	switch (PlayerStatus) {
	// 		case PlayerCondition.Normal:
	// 			if (PlayerIsGround) {
	// 				if (Input.GetKeyDown (KeyCode.W)) {
	// 					//Player.transform.Translate(new Vector2(0,JumpForce)*Time.deltaTime);
	// 					playerBody.AddForce (new Vector2 (0, JumpForce), ForceMode2D.Impulse);
	// 				}
	// 			}
	// 			break;
	// 		case PlayerCondition.OnWater:
	// 			if (Input.GetKeyDown (KeyCode.W)) {
	// 				//Player.transform.Translate(new Vector2(0,JumpForce)*Time.deltaTime);
	// 				playerBody.AddForce (new Vector2 (0, JumpForce / 5), ForceMode2D.Impulse);
	// 			}
	// 			break;

	// 		case PlayerCondition.OnSpace:
	// 			if (Input.GetKey (KeyCode.W)) {
	// 				transform.Rotate (transform.rotation.x, transform.rotation.y, transform.rotation.z - 1);
	// 			}
	// 			break;
	// 	}
	// }

	// private void VelocityControl () {
	// 	switch (PlayerStatus) {
	// 		case PlayerCondition.Normal:
	// 			if (PlayerVelocity.y < maxFallVelocity) {
	// 				PlayerBody.velocity = new Vector2 (PlayerBody.velocity.x, maxFallVelocity);
	// 			}
	// 			break;
	// 	}
	// }

	// //set player animation
	// private void setPlayerAnim () {
	// 	PlayerAnim.SetBool ("IsGround", PlayerIsGround);
	// 	PlayerAnim.SetBool ("Jump", Playerjumping);
	// 	PlayerAnim.SetBool ("Squat", PlayerSquating);
	// 	PlayerAnim.SetBool ("LookUp", PlayerLookUp);
	// 	PlayerAnim.SetBool ("Move", PlayerMoving);
	// }
}