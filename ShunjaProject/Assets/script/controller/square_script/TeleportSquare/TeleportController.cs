﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportController : MonoBehaviour {

	public Transform TeleportSquare;
	public bool inside = false;
	public bool playerInside = false;
	public string myName;
	public string TargetBoxCollision;

	private GameObject fakePlayer;
	private GameObject collisionObject = null;

	public PlayerMovementController fakePlayerController;

	void Awake () {
		inside = false;
		fakePlayer = GameObject.FindGameObjectWithTag ("" + myName);
		fakePlayerController = fakePlayer.GetComponent<PlayerMovementController> ();
	}

	public void setFakePlayerSpriteSoftOrder (string name) {
		fakePlayerController.PlayerSprite.sortingLayerName = "" + name;
	}

	public void setTeleportSquare (Transform holderSquare) {
		Vector2 scale = TeleportSquare.localScale;
		Vector2 holderScale = holderSquare.localScale;
		TeleportSquare.localScale = new Vector2 ((scale.x / holderScale.x) * 80, scale.y);
	}

	void Start () {
		if (fakePlayer == null) {
			fakePlayer = GameObject.FindGameObjectWithTag ("" + myName);
			fakePlayerController = fakePlayer.GetComponent<PlayerMovementController> ();
		}
	}

	public void Update () {
		if (inside == false) {
			fakePlayerController.PlayerSprite.sortingLayerName = "player_fake";
		}

		if (collisionObject == null) {
			playerInside = false;
		}
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			fakePlayerController.PlayerSprite.sortingLayerName = "player";
			inside = true;
		}
		if (collision.gameObject.tag == "" + TargetBoxCollision) {
			collisionObject = collision.gameObject;
			playerInside = true;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			fakePlayerController.PlayerSprite.sortingLayerName = "player";
			inside = true;
		}
		if (collision.gameObject.tag == "" + TargetBoxCollision) {
			collisionObject = collision.gameObject;
			playerInside = true;
		} else {
			collisionObject = null;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			inside = false;
		}
		if (collision.gameObject.tag == "" + TargetBoxCollision) {
			playerInside = false;
		}
	}
}