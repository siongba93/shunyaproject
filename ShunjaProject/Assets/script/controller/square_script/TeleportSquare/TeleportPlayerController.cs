﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityCursorControl;
using UnityEngine;

public class TeleportPlayerController : MonoBehaviour {
	public static CurrentPlayer currentPlayer;
	public TeleportHolderController holder;
	public GameObject Player;
	public GameObject Player_Left;
	public GameObject Player_Right;

	public DrawSquareController drawController;

	public Vector2 teleportSquare;

	void Start () {
		drawController = GameObject.FindWithTag ("DrawSquareController").GetComponent<DrawSquareController> ();
		Player = GameObject.FindWithTag ("Player");
	}

	public void updatePlayerPosition () {
		String name = drawController.Player.tag;
		if (drawController.FirstSquareStore != null) {
			teleportSquare = drawController.FirstSquareStore.transform.localScale;
			holder = drawController.FirstSquareStore.GetComponent<TeleportHolderController> ();
		} else {
			teleportSquare = new Vector2 (0, 0);
			holder = null;
			return;
		}

		Vector3 PlayerLeft = Player_Left.transform.position;
		Vector3 PlayerRight = Player_Right.transform.position;

		#region SetFakePlayerPosition
		if (holder == null) {
			return;
		}
		switch (holder.teleportState) {
			case TeleportState.Left:
				Player.transform.position = PlayerRight;
				break;
			case TeleportState.Right:
				Player.transform.position = PlayerLeft;
				break;
		}
		#endregion
	}

	// /5 = *2/10
	public void updateFakePlayerInsidePosition () {
		Player_Right.transform.position = new Vector2 (this.Player.transform.position.x + teleportSquare.x / 5 - 1.6f, this.Player.transform.position.y);
		Player_Left.transform.position = new Vector2 (this.Player.transform.position.x - teleportSquare.x / 5 + 1.6f, this.Player.transform.position.y);
	}

	public void updateFakePlayerOutsidePosition () {
		Player_Right.transform.position = new Vector2 (this.Player.transform.position.x + teleportSquare.x / 5 + 1.6f, this.Player.transform.position.y);
		Player_Left.transform.position = new Vector2 (this.Player.transform.position.x - teleportSquare.x / 5 - 1.6f, this.Player.transform.position.y);
	}
}