﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataBase;

public class ScanColorSquareController : MonoBehaviour
{
	public ColorSquareState colliderState;
	public SpriteRenderer image;

	void OnTriggerEnter2D (Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer ("ColorSquare")) {
			colliderState = collision.gameObject.GetComponent<ColorSquareController> ().state;
			_checkEnterCollisionSquare ();
		}
	}

	void OnTriggerExit2D (Collider2D collision)
	{
		if (collision.gameObject.layer == LayerMask.NameToLayer ("ColorSquare")) {
			colliderState = ColorSquareState.EMPTY;
		}
	}

	private void  _checkEnterCollisionSquare ()
	{
		switch (colliderState) {
		case ColorSquareState.RED:
			image.color = Color.red;
			break;
		case ColorSquareState.ORANGE:
			image.color = new Color (255, 100, 0, 255);
			break;
		case ColorSquareState.YELLOW:
			image.color = Color.yellow;
			break;
		case ColorSquareState.GREEN:
			image.color = Color.green;
			break;
		case ColorSquareState.BLUE:
			image.color = Color.blue;
			break;
		case ColorSquareState.PINK:
			image.color = new Color (255, 0, 150, 255);
			break;
		case ColorSquareState.PURPLE:
			image.color = new Color (150, 0, 255, 255);
			break;
		}
	}
}
