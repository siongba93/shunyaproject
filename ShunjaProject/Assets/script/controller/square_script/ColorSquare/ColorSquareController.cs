﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class ColorSquareController : MonoBehaviour {
	public SpriteRenderer sprite;
	public ColorSquareState state;
	public bool Fixed;
	public int redNum;
	public int orangeNum;
	public int yellowNum;
	public int greenNum;
	public int blueNum;
	public int pinkNum;
	public int purpleNum;

	public bool first = false;

	void Start () {
		_startCheckColor ();
	}
	void Update () {
		if (!Fixed) {
			_changeColor ();
		}
	}

	void OnTriggerEnter2D (Collider2D collision) {
		Debug.Log ("collision02");
		if (!Fixed) {
			if (collision.gameObject.layer == LayerMask.NameToLayer ("SquareColor")) {
				Debug.Log ("collision01");
				_checkEnterCollisionSquare (collision.gameObject.GetComponent<ColorSquareController> ());
			}
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (!Fixed) {
			if (collision.gameObject.layer == LayerMask.NameToLayer ("SquareColor")) {
				_checkExitCollisionSquare (collision.gameObject.GetComponent<ColorSquareController> ());
			}
		}
	}

	private void _checkEnterCollisionSquare (ColorSquareController controller) {
		switch (controller.state) {
			case ColorSquareState.RED:
				redNum += 1;
				break;
			case ColorSquareState.ORANGE:
				orangeNum += 1;
				break;
			case ColorSquareState.YELLOW:
				yellowNum += 1;
				break;
			case ColorSquareState.GREEN:
				redNum += 1;
				break;
			case ColorSquareState.BLUE:
				blueNum += 1;
				break;
			case ColorSquareState.PINK:
				pinkNum += 1;
				break;
			case ColorSquareState.PURPLE:
				purpleNum += 1;
				break;
		}
	}

	private void _checkExitCollisionSquare (ColorSquareController controller) {
		switch (controller.state) {
			case ColorSquareState.RED:
				redNum -= 1;
				break;
			case ColorSquareState.ORANGE:
				orangeNum -= 1;
				break;
			case ColorSquareState.YELLOW:
				yellowNum -= 1;
				break;
			case ColorSquareState.GREEN:
				greenNum -= 1;
				break;
			case ColorSquareState.BLUE:
				blueNum -= 1;
				break;
			case ColorSquareState.PINK:
				pinkNum -= 1;
				break;
			case ColorSquareState.PURPLE:
				purpleNum -= 1;
				break;
		}
	}

	public void _startCheckColor () {
		switch (state) {
			case ColorSquareState.RED:
				sprite.color = Color.red;
				break;
			case ColorSquareState.ORANGE:
				sprite.color = new Color (255, 100, 0, 255);
				break;
			case ColorSquareState.YELLOW:
				sprite.color = Color.yellow;
				break;
			case ColorSquareState.GREEN:
				sprite.color = Color.green;
				break;
			case ColorSquareState.BLUE:
				sprite.color = Color.blue;
				break;
			case ColorSquareState.PINK:
				sprite.color = new Color (255, 0, 150, 255);
				break;
			case ColorSquareState.PURPLE:
				sprite.color = new Color (150, 0, 255, 255);
				break;
		}
	}

	private void _changeColor () {
		if (redNum >= 3 || orangeNum >= 3 || yellowNum >= 3 ||
			greenNum >= 3 || blueNum >= 3 || pinkNum >= 3 || purpleNum >= 3) {
			sprite.color = Color.black;
			return;
		}

		if (redNum >= 2) {
			sprite.color = Color.red;
		}

		if (orangeNum >= 2) {
			sprite.color = new Color (255, 100, 0, 255);
		}

		if (yellowNum >= 2) {
			sprite.color = Color.yellow;
		}

		if (greenNum >= 2) {
			sprite.color = Color.green;
		}

		if (blueNum >= 2) {
			sprite.color = Color.blue;
		}

		if (pinkNum >= 2) {
			sprite.color = new Color (255, 0, 150, 255);
		}

		if (purpleNum >= 2) {
			sprite.color = new Color (150, 0, 255, 255);
		}

		if (redNum < 2 && orangeNum < 2 && yellowNum < 2 && greenNum < 2 && blueNum < 2 && pinkNum < 2 && purpleNum < 2) {
			sprite.color = Color.white;
		}
	}
}