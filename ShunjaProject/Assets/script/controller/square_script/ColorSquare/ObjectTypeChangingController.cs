﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTypeChangingController : MonoBehaviour
{

	public SpriteRenderer Image;
	public Sprite NormalImage;
	public Sprite BlackImage;
	public Sprite WhiteImage;
	public Sprite OverlapImage;
	public bool Inside;
	public bool Overlap = false;
	public Collider2D other;

	void Update ()
	{
		if (Inside && other !=null) {
			string collisionName = other.gameObject.name;
			if (collisionName == "OverlapSquare") {
				Inside = true;
				Image.sprite = OverlapImage;
				Overlap = true;
				return;
			}	
			if (Overlap == false) {
				switch (collisionName) {
				case "WhiteSquare":
					Image.sprite = WhiteImage;
					break;
				case "BlackSquare":
					Image.sprite = BlackImage;
					break;
				}
			}
		} else if(other == null){
			Image.sprite = NormalImage;
		}
	}

	void OnTriggerEnter2D (Collider2D collision)
	{
		string collisionName = collision.gameObject.name;
		if (collisionName == "WhiteSquare" ||
		   collisionName == "BlackSquare" ||
		   collisionName == "OverlapSquare") {
			Inside = true;
			other = collision;
		}
	}

	void OnTriggerStay2D (Collider2D collision)
	{
		string collisionName = collision.gameObject.name;
		if (collisionName == "WhiteSquare" ||
		    collisionName == "BlackSquare" ||
		    collisionName == "OverlapSquare") {
			Inside = true;
			other = collision;
		}
	}

	void OnTriggerExit2D (Collider2D collision)
	{
		string collisionName = collision.gameObject.name;
		if (collisionName == "OverlapSquare") {
			Overlap = false;
		}
		if(collisionName == "WhiteSquare"||
			collisionName =="BlackSquare"){
			if(other == null)
			Image.sprite = NormalImage;
		}
	
	}
}
