﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapObjectController : MonoBehaviour {
	public Vector2 square01;
	public Vector2 square02;
	public Transform Object01;
	public Transform Object02;
	public LayerMask mask;
	public Texture Image01;
	public Texture Image02;
	
	// Update is called once per frame
	void Update () {
		Object01.position = square01;
		Object02.position = square02;
		if(Physics2D.OverlapArea(square01,square02,mask, -Mathf.Infinity, Mathf.Infinity))
		{
			Debug.Log ("inside");
		}
	}

	void OnGUI (){

	}
}
