﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class BlackWhiteSquareController : MonoBehaviour {

	public SpriteMask mask;
	public string overlapName;
	public SpriteRenderer image;
	public ScanBlackWhiteSquareController Left;
	public ScanBlackWhiteSquareController Right;
	public ScanBlackWhiteSquareController Up;
	public ScanBlackWhiteSquareController Down;

	public Animator anim;

	[Header ("PhysicsIgnoreFeature")]
	public BoxCollider2D myCollision;
	public GameObject collisionObject;
	public bool playerInside = false;

	void Update () {
		// if (playerInside == true) {
		// 	Physics2D.IgnoreLayerCollision (11, 21, false);
		// 	Physics2D.IgnoreLayerCollision (12, 21, false);
		// 	Physics2D.IgnoreLayerCollision (15, 21, false);
		// } else {
		// 	Physics2D.IgnoreLayerCollision (11, 21, true);
		// 	Physics2D.IgnoreLayerCollision (12, 21, true);
		// 	Physics2D.IgnoreLayerCollision (15, 21, true);
		// }
	}

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.name == "" + overlapName) {
			Transform body = collision.gameObject.transform;
			Vector2 Scale = body.localScale;
		}
	}

	private void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = true;
			// Debug.Log ("PlayerInside");
		}
	}

	private void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player) {
			playerInside = false;
			// Debug.Log ("PlayerOutside");
		}
	}

	public void onCompleteAnimationDestroy () {
		Destroy (gameObject);
	}

	public void destroy () {
		image.enabled = false;
		anim.SetFloat ("speed", 1);
	}
}