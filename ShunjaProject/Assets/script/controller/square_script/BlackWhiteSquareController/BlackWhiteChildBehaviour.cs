﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class BlackWhiteChildBehaviour : MonoBehaviour {
	public bool WhiteInside;
	public bool BlackInside;

	public GameObject collisionObject;

	void Update () {
		if (collisionObject != null) {
			if (collisionObject.activeInHierarchy == false) {
				WhiteInside = false;
				BlackInside = false;
			}
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWhite) {
			WhiteInside = true;
			collisionObject = collision.gameObject;
		} else if (collision.gameObject.tag == Model.SquareBlack) {
			BlackInside = true;
			collisionObject = collision.gameObject;

		}

	}
	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWhite) {
			WhiteInside = false;
		} else if (collision.gameObject.tag == Model.SquareBlack) {
			BlackInside = false;
		}
	}
}