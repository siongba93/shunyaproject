﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class ScanBlackWhiteSquareController : MonoBehaviour {
	public SpriteRenderer sprite;
	public Transform myBody;
	private Vector3 worldPos;
	public string squareType;
	public bool Inside;

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == squareType) {
			sprite.color = Color.red;
			Inside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == squareType) {
			sprite.color = Color.white;
			Inside = false;
		}
	}
}