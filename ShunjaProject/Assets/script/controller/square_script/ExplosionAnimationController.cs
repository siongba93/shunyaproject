﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAnimationController : MonoBehaviour {

	public BlackWhiteSquareController controller;

	// Update is called once per frame
	public void _onCompleteAnimation () {
		controller.onCompleteAnimationDestroy ();
	}
}