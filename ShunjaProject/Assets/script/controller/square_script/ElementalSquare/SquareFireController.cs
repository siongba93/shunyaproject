﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class SquareFireController : MonoBehaviour {

	public Vector3 initialScale = new Vector3 (1, 1, 0);
	public float smoothTime = 1f;
	private Vector3 velocity3D = Vector3.zero;

	void Update () {
		this.transform.localScale = Vector3.SmoothDamp (this.transform.localScale, initialScale, ref velocity3D, smoothTime);
	}

}