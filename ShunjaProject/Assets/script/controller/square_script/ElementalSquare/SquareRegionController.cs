﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class SquareRegionController : MonoBehaviour {

	public SpriteRenderer image;
	public string squareName;
	public Sprite imageGlow;
	public Sprite originalImage;

	void Start () {
		image.sprite = originalImage;
		float myOpacity = image.color.a;
		myOpacity = 0.25f;
		image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareHolder) {
			float myOpacity = image.color.a;
			myOpacity = 0.5f;
			image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
			image.sprite = imageGlow;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareHolder) {
			float myOpacity = image.color.a;
			myOpacity = 0.25f;
			image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
			image.sprite = originalImage;
		}
	}
}