﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class WindSquareRegionController : MonoBehaviour {

	public SpriteRenderer image;
	public string squareName;
	public float horizontalForce;
	public float verticalForce;
	public Sprite imageGlow;
	public Sprite originalImage;

	void Start () {
		image.sprite = originalImage;
		float myOpacity = image.color.a;
		myOpacity = 0.75f;
		image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == squareName) {
			float myOpacity = image.color.a;
			myOpacity = 1f;
			SquareData.windHorizontalForce += horizontalForce;
			SquareData.windVerticalForce += verticalForce;
			image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
			image.sprite = imageGlow;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == squareName) {
			float myOpacity = image.color.a;
			myOpacity = 0.75f;
			SquareData.windHorizontalForce = 0;
			SquareData.windVerticalForce = 0;
			image.color = new Color (image.color.r, image.color.g, image.color.b, myOpacity);
			image.sprite = originalImage;
		}
	}
}