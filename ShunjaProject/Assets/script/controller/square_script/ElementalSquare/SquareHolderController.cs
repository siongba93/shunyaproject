﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public enum SquareState
{
    hide,
    appear,
    destroy
}

public class SquareHolderController : MonoBehaviour
{

    public SpriteRenderer image;

    public SquareState state = SquareState.hide;
    public GameObject square;
    public GameObject EarthSquare;
    public GameObject WaterSquare;
    public GameObject FireSquare;
    public GameObject WindSquare;
    public BoxCollider2D myCollision;
    public float opacityImage;

    public bool canDraw = false;

    public bool hasCollisionObject;
    public bool isEnterCollision = false;
    public bool canFadeOut = false;

    public bool isExist;
    void Start()
    {
        state = SquareState.hide;
        isEnterCollision = false;
        hasCollisionObject = false;
        isExist = false;
    }

    // public void fadeOut () {
    // 	canFadeOut = true;
    // }

    void OnTriggerEnter2D(Collider2D collision)
    {
        isEnterCollision = true;
        string collisionTag = collision.gameObject.tag;
        if ((collisionTag == Model.EarthSquareRegion || collisionTag == Model.WaterSquareRegion || collisionTag == Model.FireSquareRegion || collisionTag == Model.WindSquareRegion) &&
            hasCollisionObject == false && isExist == false)
        {
            this.transform.parent = collision.gameObject.transform;
            canFadeOut = false;
            canDraw = true;
            state = SquareState.appear;
            hasCollisionObject = true;
            if (collisionTag == Model.EarthSquareRegion)
            {
                square = EarthSquare;
            }
            else if (collisionTag == Model.WaterSquareRegion)
            {
                square = WaterSquare;
            }
            else if (collisionTag == Model.FireSquareRegion)
            {
                square = FireSquare;
            }
            else if (collisionTag == Model.WindSquareRegion)
            {
                square = WindSquare;
            }
            GameObject spawnSquare = (GameObject)Instantiate(square, Vector3.zero, Quaternion.identity);
            spawnSquare.transform.parent = this.transform;
            spawnSquare.transform.localScale = new Vector3(1, 0f, 0);
            spawnSquare.transform.localPosition = new Vector3(0, -0.1f, 0);
        }
    }

    public void Update()
    {
        if (hasCollisionObject == false && isEnterCollision == true)
        {
            isExist = true;
            canFadeOut = true;
            canDraw = false;
            myCollision.enabled = false;
        }
        opacityImage = image.color.a;

        if (canFadeOut)
        {
            opacityImage = Mathf.Lerp(opacityImage, 0, 0.1f);
            image.color = new Color(image.color.r, image.color.g, image.color.b, opacityImage);
        }

        if (Mathf.Abs(opacityImage - 0) < 0.01f)
        {
            Destroy(this.gameObject);
        }
    }

}
