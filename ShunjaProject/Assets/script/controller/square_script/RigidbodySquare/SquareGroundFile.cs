﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareGroundFile : MonoBehaviour {
	private float speed=1;
	private Vector3 DestinationPos;
	void Start(){
		Vector3 childPos = this.transform.GetChild (0).position;
		DestinationPos = Vector3.zero + childPos;
	}
	void Update () {
		if (this.transform.parent.tag == "ChainCircle") {
			this.transform.localPosition = Vector3.Lerp (this.transform.localPosition, DestinationPos, speed * Time.deltaTime);
		}
	//	this.transform.position = Vector3.Lerp(this.transform.position,new Vector2 (0, 0),speed*Time.deltaTime);
	}
}
