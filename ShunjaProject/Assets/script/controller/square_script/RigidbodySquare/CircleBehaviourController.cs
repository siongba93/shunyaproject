﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class CircleBehaviourController : MonoBehaviour {
	public float speed = 1;

	public GameObject Image;

	void OnTriggerEnter2D (Collider2D collision) {
		Debug.Log ("White No In");
		if (collision.gameObject.tag == Model.SquareWhite) {
			Debug.Log ("White In");
			_colorChanging (Color.red);
			collision.transform.parent = this.transform;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.transform.parent != this.transform) {
			_colorChanging (Color.green);
		}
		//			float PosX = obj.transform.localPosition.y;
		//			PosX= Mathf.Lerp (obj.transform.localPosition.y, 0, speed * Time.deltaTime);
		//			obj.transform.localPosition = new Vector2 (obj.transform.localPosition.x, PosX);
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWhite) {
			Debug.Log ("White Out");
			_colorChanging (Color.green);
		}
	}

	private void _colorChanging (Color color) {
		Image.GetComponent<SpriteRenderer> ().color = color;
	}

	private void _directionChanging (int direction) {
		Image.transform.rotation = Quaternion.Euler (0, 0, direction);
	}
}