﻿using System.Collections;
using UnityEngine;

public class RotateAround : MonoBehaviour {
	public float speed;
	// Use this for initialization
	public void setMovementSpeed (float newSpeed) {
		speed = newSpeed;
	}

	// Update is called once per frame
	void Update () {
		gameObject.transform.Rotate (0, 0, speed * Time.deltaTime);
	}
}