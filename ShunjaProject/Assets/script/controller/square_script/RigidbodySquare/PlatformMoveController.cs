﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum GroundPos {
	ORIGIN,
	DESTINATION
}

enum Direction {
	Left,
	Right,
}

public class PlatformMoveController : MonoBehaviour {
	public SpriteRenderer image;
	public Transform groundPrefab;
	public Vector3 OriginPos;
	public Vector3 DestinationPos;
	public float InitialSpeed;
	public bool moveForward = true;

	public BoxCollider2D collision;
	//public IconController icon;
	private Direction CurrDirection;
	private GroundPos currentPos;
	private float _speed;

	void Start () {
		_speed = InitialSpeed;
	}

	void FixedUpdate () {
		//		setRotation ();
		platformMove ();
		switchMove ();
	}

	public void setSpeed (float newSpeed) {
		_speed = newSpeed;
	}

	private void platformMove () {
		if (moveForward == true) {
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, DestinationPos, _speed * Time.deltaTime);
		} else if (moveForward == false) {
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, OriginPos, _speed * Time.deltaTime);
		}
	}

	private void switchMove () {
		if (Vector3.Distance (groundPrefab.position, DestinationPos) < 1f) {
			moveForward = false;
		} else if (Vector3.Distance (groundPrefab.position, OriginPos) < 1f) {
			moveForward = true;
		}
	}

	//	private void setRotation ()
	//	{
	//		switch(CurrDirection){
	//		case Direction.Right:
	//			icon.DirectionChanging (180);
	//			break;
	//		case Direction.Left:
	//			icon.DirectionChanging (0);
	//			break;
	//		}
	//	}

}