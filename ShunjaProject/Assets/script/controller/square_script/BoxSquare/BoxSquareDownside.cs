﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSquareDownside : MonoBehaviour {
	public BoxSquareController controller;

	void OnTriggerStay2D (Collider2D collision)
	{
		if (collision.gameObject.tag == "Player"  ) {
			controller.OnDown = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision)
	{
		if (collision.gameObject.tag == "Player") {
			controller.OnDown = false;
		}
	}
}
