﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSquareChildController : PlayerStateController {

	public Rigidbody2D BoxSquare;
	public BoxSquareController controller;

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.gameObject.tag == "Player") {
			BoxSquare.bodyType = RigidbodyType2D.Dynamic;
			controller.enabled = true;
			controller.PlayerIsInside = true;
			Destroy (this.gameObject);
		}
	}

}
