﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSquareUpside : MonoBehaviour {
	public BoxSquareController controller;
	void OnTriggerStay2D (Collider2D collision)
	{
		if (collision.gameObject.tag == "Player"  ) {
			controller.OnUp = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision)
	{
		if (collision.gameObject.tag == "Player") {
			controller.OnUp = false;
		}
	}
}
