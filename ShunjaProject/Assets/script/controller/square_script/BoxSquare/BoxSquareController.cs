﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSquareController : PlayerStateController
{
	public bool PlayerIsInside =false;
	public bool OnDown =false;
	public bool OnUp = false;
	public Rigidbody2D body;
	public Vector2 Vec;

	void Awake(){
		PlayerIsInside = false;
		OnUp = false;
		OnDown = false;
	}

	void Start(){
		if (PlayerIsInside) {
			if (this.transform.localScale.x < 25 || this.transform.localScale.y < 25) {
				Destroy (this.gameObject);
				Debug.Log ("It is too small");
			}
		}
	}

	void Update(){
		fixBoxSpeed ();
		rigidbodyCondition ();
	}

	private void fixBoxSpeed(){
		Vec = body.velocity;
		if (body.velocity.y > 10) {
			body.velocity = new Vector2 (body.velocity.x, 10);
		} else if (body.velocity.y < -10) {
			body.velocity = new Vector2 (body.velocity.x, -10);
		}
	}

	private void rigidbodyCondition(){
		Rigidbody2D myBody = this.gameObject.GetComponent<Rigidbody2D>();
		if (OnDown) {
			if (PlayerSquating) {
				myBody.constraints = RigidbodyConstraints2D.FreezeRotation;
			} else {
				myBody.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
			}
		} else if (OnUp) {
			myBody.constraints = RigidbodyConstraints2D.FreezeRotation;
		} else {
			myBody.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
		}
	}


}
