﻿using System.Collections;
using System.Collections.Generic;
using UnityCursorControl;
using UnityEngine;

public class DestroyBlueSquareScript : MonoBehaviour {

	public SquarePlayerController controller;
	public SpriteRenderer Square;
	public float TimeRamaining;
	public float MaxTime;
	// Use this for initialization
	void Start () {
		TimeRamaining = MaxTime;
		//Invoke ("destroyScript", 0.01f);	
	}

	void Update () {
		destroyBlueSquareTimer ();
	}

	void destroyScript () {
		Destroy (controller);
	}

	private void destroyBlueSquareTimer () {
		TimeRamaining -= 1 * Time.deltaTime;
		Color SquareColor = Square.color;
		float Alpha = SquareColor.a;
		Alpha = (TimeRamaining / MaxTime) * 1;
		Square.color = new Color (SquareColor.r, SquareColor.g, SquareColor.b, Alpha);
		if (TimeRamaining < 0) {
			// DrawSquareController.RestoreSquarePower (this.gameObject);
			Destroy (this.transform.gameObject);
		}
	}

}