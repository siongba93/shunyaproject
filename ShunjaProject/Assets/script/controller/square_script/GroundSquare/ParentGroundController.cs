﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentGroundController : MonoBehaviour {

	void OnCollisionStay2D (Collision2D player) {
		if(player.gameObject.tag == "Player"){
			player.gameObject.GetComponent<Transform>().parent = this.transform;
		} 
	}
	void OnCollisionExit2D (Collision2D player) {
		if (player.gameObject.tag == "Player") {
			player.gameObject.GetComponent<Transform> ().parent = null;
		}
	}
}
