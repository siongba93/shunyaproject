﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class CameraSquareController : MonoBehaviour {

	public Camera myCamera;
	public RenderTexture rt;
	public int numX = 0;
	public int numY = 0;
	public float oriX = 0;
	public float oriY = 0;
	private RenderTexture newTexture;
	public GameObject parentObject;
	public GameObject left;
	public GameObject right;
	public GameObject up;
	public GameObject down;
	// Use this for initialization
	void Awake () {
		// this.transform.localScale /=60;
		// if (numX > numY) {
		// 	myCamera.orthographicSize = numX;
		// } else {
		// 	myCamera.orthographicSize = numY;
		// }
	}

	void Start () {
		numX = Mathf.RoundToInt (parentObject.transform.localScale.x * 10);
		numY = Mathf.RoundToInt (parentObject.transform.localScale.y * 10);
		// int localNumX = Mathf.RoundToInt (parentObject.transform.localScale.x);
		// int localNumY = Mathf.RoundToInt (parentObject.transform.localScale.y);
		oriX = parentObject.transform.localScale.x;
		oriY = parentObject.transform.localScale.y;
		//this.transform.localScale /= localNumY / 10;
		Model.cameraSquareTexture = new RenderTexture (numX, numY, 24);
		myCamera.targetTexture = Model.cameraSquareTexture;
		myCamera.orthographicSize = parentObject.transform.localScale.y / 10;
		// if (localNumX >= localNumY) {
		// 	divider = localNumX / 10;
		// } else {
		// 	divider = localNumY / 10;
		// }
		//this.transform.localScale = new Vector2 (1, 1f);
		// left.transform.parent = null;
		// right.transform.parent = null;
		// down.transform.parent = null;
		// up.transform.parent = null;

	}

	// Update is called once per frame
	void Update () {

	}
	//	myCamera.targetTexture = Model.cameraSquareTexture;

}