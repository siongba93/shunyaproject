﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlaneCameraSquareController : MonoBehaviour {

	public Material cameraMaterial;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		transform.GetComponent<Renderer> ().material.mainTexture = Model.cameraSquareTexture;
		transform.GetComponent<Renderer> ().material.SetColor ("_Color", new Color (1f, 1f, 1f, 0.1f));
	}
}