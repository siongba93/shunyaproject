﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class BackgroundSquareCheckController : MonoBehaviour {
	public Transform ImagePos;
	public SpriteRenderer ImageSprite;
	public Color OpenColor = new Color (255, 255, 0, 255);

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareGround) {
			Transform body = collision.gameObject.transform;
			Vector2 Pos = body.position;
			Vector2 Scale = body.localScale;
			if (Vector2.Distance (Pos, ImagePos.position) < 0.5f && Vector2.Distance (Scale, ImagePos.localScale) < 5) {
				ImageSprite.color = OpenColor;
				Destroy (collision.gameObject);
			}
		}
	}
}