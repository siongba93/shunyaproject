﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainController : MonoBehaviour {
	public Transform startChain;
	public Transform endChain;

	public Transform player;
	public Transform cat;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		startChain.position = player.position;
		endChain.position = cat.position;
	}
}