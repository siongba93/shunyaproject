﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class SinkingGroundController : MonoBehaviour {
	public float speed;

	public bool playerInside = false;
	public Vector3 originalPos;
	public Vector3 destinationPos;
	public SpriteRenderer crystal;
	public SinkingGroundInsideCheckingController controller;

	public void setMoveSpeed (float newSpeed) {
		speed = newSpeed;
	}

	public void setImageColor (Color color) {
		crystal.color = color;
	}

	void FixedUpdate () {
		_groundMovement ();
		controller.checking ();
	}

	private void _groundMovement () {
		if (playerInside == false) {
			this.transform.position = Vector3.MoveTowards (this.transform.position, originalPos, speed * Time.deltaTime);
		} else {
			this.transform.position = Vector3.MoveTowards (this.transform.position, destinationPos, speed * Time.deltaTime);
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player ||
			collision.gameObject.tag == Model.PlayerCheckDown) {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player ||
			collision.gameObject.tag == Model.PlayerCheckDown) {
			playerInside = false;
		}
	}
}