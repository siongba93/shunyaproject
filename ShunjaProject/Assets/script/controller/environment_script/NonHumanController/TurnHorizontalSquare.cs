﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnHorizontalSquare : MonoBehaviour {

	public Quaternion startRotation;
	public Quaternion endRotation;

	public float speed;

	void Update () {
		transform.rotation = Quaternion.Slerp (transform.rotation, endRotation, speed * Time.deltaTime);
	}

}