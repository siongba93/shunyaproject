﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingGroundController : MonoBehaviour {
	public BoxCollider2D collision;
	public Rigidbody2D body;
	public NormalInsideCheckController controller;
	public SpriteRenderer crystal;

	public void setCrystalColor (Color newColor) {
		crystal.color = newColor;
	}

	public void setBoxCollision (bool state) {
		collision.enabled = state;
	}

	public void setRigidbody2DActive () {
		body.bodyType = RigidbodyType2D.Dynamic;
	}

	public void setRigidbody2DInActive () {
		body.velocity = Vector2.zero;
		body.bodyType = RigidbodyType2D.Kinematic;
	}

	void Update () {
		controller.checking ();
	}

}