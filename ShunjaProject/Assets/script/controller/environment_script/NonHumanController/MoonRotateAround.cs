﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonRotateAround : MonoBehaviour {
	public float speed;
	public SpriteRenderer Image;

	public GameObject moonLight;

	public MoonInsideCheckingController controller;

	public void setImageColor (Color color) {
		Image.color = color;
	}
	public void setMovementSpeed (float newSpeed) {
		speed = newSpeed;
	}

	void Update () {
		controller.checking ();
		moonLight.transform.Rotate (0, 0, speed * Time.deltaTime);
	}
}