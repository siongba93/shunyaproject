﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapController : MonoBehaviour {
	public GameObject myObject;
	public PolygonCollider2D collision;

	public void setBoxCollision (bool state) {
		collision.enabled = state;
	}

}