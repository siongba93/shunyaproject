﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class CircleStoneController : MonoBehaviour {

	public Rigidbody2D myBody;
	public Vector2 myVelocity;

	void Update () {
		myVelocity = myBody.velocity;
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWind) {
			myBody.AddForce (
				new Vector2 (
					SquareData.windHorizontalForce * 5,
					SquareData.windVerticalForce * 5
				)
			);
		}
	}
}