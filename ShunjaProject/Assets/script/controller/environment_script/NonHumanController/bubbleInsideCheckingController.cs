﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bubbleInsideCheckingController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public BubbleController controller;

	public void checking () {
		_insideChecking ();
		_checkState ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
	}

	private void _checkState () {
		if (WhiteInside == false) {
			controller.setMoveSpeed (0);
			return;
		}

		if (WhiteInside == true) {
			controller.setMoveSpeed (1);
		}
	}
}