﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trap_inside_check_controller : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public TrapController controller;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		_insideChecking ();
		// _checkState ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside || RightController.WhiteInside ||
			UpController.WhiteInside || DownController.WhiteInside) {
			WhiteInside = true;
		} else {
			WhiteInside = false;
		}
		if (LeftController.BlackInside || RightController.BlackInside ||
			UpController.BlackInside || DownController.BlackInside) {
			BlackInside = true;
		} else {
			BlackInside = false;
		}
	}

	void _checkState () {
		if (WhiteInside && BlackInside) {
			controller.setBoxCollision (true);
		} else if (WhiteInside && !BlackInside) {
			controller.setBoxCollision (true);
		} else if (!WhiteInside && BlackInside) {
			controller.setBoxCollision (true);
		} else if (!WhiteInside && !BlackInside) {
			controller.setBoxCollision (false);
		}
	}
}