﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalInsideCheckController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public FallingGroundController controller;

	// Update is called once per frame
	public void checking () {
		_insideChecking ();
		_checkState ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
		if (LeftController.BlackInside == true || RightController.BlackInside == true ||
			UpController.BlackInside == true || DownController.BlackInside == true) {
			BlackInside = true;
		}
		if (LeftController.BlackInside == false && RightController.BlackInside == false &&
			UpController.BlackInside == false && DownController.BlackInside == false) {
			BlackInside = false;
		}
	}

	void _checkState () {
		if (WhiteInside == false && BlackInside == false) {
			controller.setBoxCollision (false);
			controller.setRigidbody2DInActive ();
			controller.setCrystalColor (Color.grey);
			return;
		}

		if (WhiteInside == true && BlackInside == false) {
			controller.setBoxCollision (true);
			controller.setRigidbody2DActive ();
			controller.setCrystalColor (Color.green);
		} else if (WhiteInside == false && BlackInside == true) {
			controller.setBoxCollision (true);
			controller.setRigidbody2DActive ();
			controller.setCrystalColor (Color.black);
		}

	}
}