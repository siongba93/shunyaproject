﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockCircleMovementController : MonoBehaviour {
	public Transform groundPrefab;
	public Vector3 OriginPos;
	public Vector3 DestinationPos;
	public float speed;

	public bool moveForward = true;

	//public IconController icon;
	private Direction CurrDirection;
	private GroundPos currentPos;

	void FixedUpdate () {
		//		setRotation ();
		platformMove ();
		switchMove ();
	}

	private void platformMove () {
		if (moveForward == true) {
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, DestinationPos, speed * Time.deltaTime);
		} else if (moveForward == false) {
			groundPrefab.position = Vector3.MoveTowards (groundPrefab.position, OriginPos, speed * Time.deltaTime);
		}
	}

	private void switchMove () {
		if (Vector3.Distance (groundPrefab.position, DestinationPos) < 1f) {
			moveForward = false;
		} else if (Vector3.Distance (groundPrefab.position, OriginPos) < 1f) {
			moveForward = true;
		}
	}

}