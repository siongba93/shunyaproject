﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderController : PlayerStateController {

	public BoxCollider2D groundCollider;

	public GameObject player;
	public CircleCollider2D playerCircle;
	private BoxCollider2D _playerCollision;
	private BoxCollider2D _checkGroundCollision;

	public LadderGroundController ladderGroundController;
	public bool ignoreCollision;

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}

	void Start () {
		_playerCollision = player.GetComponent<BoxCollider2D> ();
		_checkGroundCollision = player.GetComponent<PlayerMainController> ().checkGround;
	}

	public void Update () {
		if (PlayerClimbing || ladderGroundController.insideGround) {
			ignoreCollision = true;
			Physics2D.IgnoreCollision (playerCircle, groundCollider, true);
			Physics2D.IgnoreCollision (_playerCollision, groundCollider, true);
			Physics2D.IgnoreCollision (_checkGroundCollision, groundCollider, true);
		} else {
			ignoreCollision = false;
			Physics2D.IgnoreCollision (playerCircle, groundCollider, false);
			Physics2D.IgnoreCollision (_playerCollision, groundCollider, false);
			Physics2D.IgnoreCollision (_checkGroundCollision, groundCollider, false);
		}
	}

}