﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class LadderGroundController : MonoBehaviour {

	public bool insideGround;

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.PlayerStateCheck) {
			insideGround = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.PlayerStateCheck) {
			insideGround = false;
		}
	}
}