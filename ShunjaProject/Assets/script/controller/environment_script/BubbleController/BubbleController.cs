﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class BubbleController : MonoBehaviour {
	public Vector2 BubbleScale;
	public ParticleSystem dust;
	public SpriteRenderer[] image;
	public AudioSource myAudio;
	public float Timer;
	public bool firstEnter = false;
	public CircleCollider2D collision;

	public Vector2 moveUp = new Vector2 (0, 1);
	public float speed = 1;

	public float maxHigh;
	public bubbleInsideCheckingController controller;

	public void setMoveSpeed (float newSpeed) {
		speed = newSpeed;
	}

	void Start () {
		firstEnter = false;
		Timer = this.transform.localScale.x;
	}

	void Update () {
		controller.checking ();
		_destroyMaxHigh ();
		_moveUp ();
	}

	private void _moveUp () {
		this.transform.Translate (moveUp * speed * Time.deltaTime);
	}

	private void _destroyMaxHigh () {
		if (this.transform.position.y > maxHigh) {
			Destroy (this.gameObject);
		}
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if ((other.gameObject.tag == Model.Player || other.gameObject.tag == Model.PlayerCheckDown) && firstEnter == false) {
			firstEnter = true;
			image[0].enabled = false;
			image[1].enabled = false;
			collision.enabled = false;
			dust.Play ();
			myAudio.Play ();
			StartCoroutine ("destroyObject");
		}
	}

	public IEnumerator destroyObject () {
		yield return new WaitForSeconds (dust.main.startLifetimeMultiplier);
		Destroy (this.gameObject);
	}
}