﻿using System.Collections;
using UnityEngine;

public class BubbleFire4 : MonoBehaviour {
	public GameObject bubble;
	public float delaytimer;
	public float timer;
	public float num;
	public float fireMax;
	public float fireMin;
	// Use this for initialization
	void Start () {
		num = 0;
	}

	// Update is called once per frame
	void Update () {
		timer -= 1f * Time.deltaTime;
		if (timer <= 0) {
			Vector3 bubblePos = this.transform.position;
			float scale = Random.Range (0.5f, 1f);
			bubble.transform.localScale = new Vector2 (scale, scale);
			GameObject bubbleGO = (GameObject) Instantiate (bubble, bubblePos, transform.rotation);
			timer = delaytimer;
			bubbleGO.GetComponent<Rigidbody2D> ().velocity = transform.rotation * new Vector2 (Random.Range (fireMax, fireMin), 0);
		}
	}
}