﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SourceStarController : MonoBehaviour {

	public GameObject myObject;
	public GameObject crystalObject;
	public SpriteRenderer mySprite;
	public SpriteRenderer crystal;
	public Transform transformObject;

	public bool active = false;
	public bool calculated = false;

	public SourceStarCheckingInsideController controller;

	public Transform myTransform {
		get {
			return transformObject;
		}
	}

	public bool myCalculated {
		get {
			return calculated;
		}
	}

	public void setCalculated (bool state) {
		calculated = state;
	}

	public void Update () {
		controller.checking ();
	}

	public void setActive (bool state) {
		active = state;
	}

	public void setCrystal (Color color) {
		crystal.color = color;
	}

	public void setMySpriteColor (Color color) {
		mySprite.color = color;
	}

	public void setCrystalEnabled (bool state) {
		crystalObject.SetActive (state);
		active = state;
		mySprite.color = Color.grey;
		controller.setActive (false);
	}
}