﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class KeyController : PlayerStateController {

	public GameObject Key;
	public Rigidbody2D myBody;
	public Vector2 velocity;
	public float maxVelocityY;
	public float minVelocityY;
	public bool isForceAdd;

	void Start () {
		minVelocityY = -3f;
		maxVelocityY = 10f;
		isForceAdd = false;
	}

	void Update () {
		velocity = myBody.velocity;

		if (PlayerHoldingKey == true) {
			Key.SetActive (false);
			Destroy (this.gameObject);
		}
	}

	void FixedUpdate () {
		if (myBody.velocity.y > maxVelocityY) {
			myBody.velocity = new Vector2 (myBody.velocity.x, maxVelocityY);
		} else if (myBody.velocity.y < minVelocityY) {
			myBody.velocity = new Vector2 (myBody.velocity.x, minVelocityY);
		}

		if (isForceAdd) {
			windSquareAddForce ();
		}
	}

	public void windSquareAddForce () {
		myBody.AddForce (
			new Vector2 (
				SquareData.windHorizontalForce * 5,
				SquareData.windVerticalForce * 5
			)
		);
	}

	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == Model.KeyDoor) {
			Key.SetActive (false);
			Destroy (this.gameObject);
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWind) {
			isForceAdd = true;
			minVelocityY = -3f;
		} else if (collision.gameObject.tag == Model.SquareWater) {
			minVelocityY = -0.5f;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareWater ||
			collision.gameObject.tag == Model.SquareWind ||
			collision.gameObject.tag == Model.SquareFire) {
			isForceAdd = false;
			minVelocityY = -3f;
		}
	}

}