﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyHolderController : PlayerStateController {

	public SpriteRenderer key;

	public GameObject keyObject;

	void Start () {
		key.enabled = false;
		PlayerHoldingKey = false;
		PlayerCanHoldKey = false;
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.E) && PlayerCanHoldKey == true && PlayerHoldingKey == false) {
			key.enabled = true;
			PlayerHoldingKey = true;
		} else if (Input.GetKeyDown (KeyCode.E) && PlayerHoldingKey == true) {
			key.enabled = false;
			PlayerHoldingKey = false;
			GameObject Key = Instantiate (keyObject, this.transform.position, Quaternion.identity);
			if (PlayerDirection == 1) {
				Key.transform.localRotation = Quaternion.Euler (0, 0, 0);
			} else {
				Key.transform.localRotation = Quaternion.Euler (0, 180, 0);
			}
		}
	}

	public void setKeyEnabled (bool state) {
		key.enabled = state;
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == "key") {
			Debug.Log ("key");
			PlayerCanHoldKey = true;
		}
		if (collision.gameObject.tag == "KeyDoor" && PlayerHoldingKey == true) {
			key.enabled = false;
			PlayerCanHoldKey = false;
		}
	}

}