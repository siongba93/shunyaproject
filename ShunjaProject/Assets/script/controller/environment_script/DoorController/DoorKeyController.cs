﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class DoorKeyController : PlayerStateController {
	public GameObject Door;

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == "KeyHolder") {
			if (PlayerHoldingKey == true) {
				Door.SetActive (false);
			}
		}
		if (collision.gameObject.tag == Model.Key) {
			Door.SetActive (false);
		}
	}
	void OnCollisionEnter2D (Collision2D collision) {
		if (collision.gameObject.tag == Model.Key) {
			Door.SetActive (false);
		}
	}
}