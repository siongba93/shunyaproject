﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class ButtonGroundController : MonoBehaviour {

	public Animator anim;
	public SwitchState state;

	#region MovingGround
	public Transform movingGround;
	public Vector3 RedPos;
	public Vector3 BluePos;
	public float speed;
	#endregion

	void Start () {
		anim.speed = 0;
	}

	void Update () {
		_movingGroundController ();
	}

	private void _movingGroundController () {
		switch (state) {
			case SwitchState.Red:
				movingGround.position = Vector3.MoveTowards (movingGround.position, RedPos, speed * Time.deltaTime);
				break;
			case SwitchState.Blue:
				movingGround.position = Vector3.MoveTowards (movingGround.position, BluePos, speed * Time.deltaTime);
				break;
		}
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player" ||
			collision.gameObject.tag == "Enemy" ||
			collision.gameObject.tag == "SquareEarth") {
			Debug.Log ("01" + collision.gameObject.name);
			anim.speed = 1;
			anim.Play (0);
			state = SwitchState.Red;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player" ||
			collision.gameObject.tag == "Enemy" ||
			collision.gameObject.tag == "SquareEarth") {
			Debug.Log ("02" + collision.gameObject.name);
			anim.speed = 1;
			state = SwitchState.Red;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player" ||
			collision.gameObject.tag == "Enemy" ||
			collision.gameObject.tag == "SquareEarth") {
			Debug.Log ("03" + collision.gameObject.name);
			anim.speed = -1;
			anim.Play (0);
			state = SwitchState.Blue;
		}
	}
}