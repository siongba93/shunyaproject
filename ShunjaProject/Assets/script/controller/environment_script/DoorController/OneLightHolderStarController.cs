﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneLightHolderStarController : MonoBehaviour {

	public SourceStarController sourceController;
	public LightStarKeyController lightStarController;

	public GameObject door;
	public float distance;
	public bool firstAchieve;
	public float minDistance;

	public int keyHolder = 0;
	public int targetHolder = 4;

	private void _calculateLightStar () {
		distance = Vector2.Distance (sourceController.myTransform.position, lightStarController.myTransform.position);
	}

	private void _lightStarMovement () {
		float speed = lightStarController.mySpeed;
		lightStarController.myTransform.position = Vector3.Lerp (lightStarController.myTransform.position, sourceController.myTransform.position, speed * Time.deltaTime);
	}

	void FixedUpdate () {
		bool source0 = sourceController.active;

		if (source0 == true) {
			_lightStarMovement ();
		}
		_checkDestination ();
		_checkDoor ();
	}

	private void _checkDestination () {
		if (Vector2.Distance (lightStarController.myTransform.position, sourceController.myTransform.position) < 0.1 && firstAchieve == false) {
			sourceController.setCrystalEnabled (false);
			firstAchieve = true;
			keyHolder += 1;
		}
	}

	public void _checkDoor () {
		if (keyHolder == targetHolder) {
			door.SetActive (false);
			lightStarController.setCrystalEnabled (false);
		}
	}
}