﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoLighterController : MonoBehaviour {
	private LighterState _state = LighterState.OFF;
	public float startTime = 3;
	public float offTime = 3;
	public float onTime = 3;
	public GameObject lighter;
	public SpriteRenderer image;

	void Start () {
		Invoke ("_setUpLighter", startTime);
	}
	// Update is called once per frame
	private void _setUpLighter () {
		if (_state == LighterState.ON) {
			Invoke ("_offLighter", offTime);
			image.color = Color.white;
		} else {
			Invoke ("_onLighter", onTime);
			image.color = Color.black;
		}
	}

	private void _onLighter () {
		Invoke ("_setUpLighter", 0);
		_state = LighterState.ON;
		lighter.SetActive (true);
	}

	private void _offLighter () {
		lighter.SetActive (false);
		_state = LighterState.OFF;
		Invoke ("_setUpLighter", 0);
	}

}