﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SourceStarCheckingInsideController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public SourceStarController controller;

	public bool active = true;

	public void setActive (bool state) {
		active = state;
	}

	public void checking () {
		_insideChecking ();
		if (active) {
			_checkState ();
		}
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
	}

	private void _checkState () {
		if (WhiteInside == false) {
			controller.setCrystal (Color.grey);
			controller.setMySpriteColor (Color.grey);
			controller.setActive (false);
			controller.setCalculated (false);
			return;
		}

		if (WhiteInside == true) {
			controller.setCrystal (Color.green);
			controller.setMySpriteColor (Color.white);
			controller.setActive (true);
		}
	}
}