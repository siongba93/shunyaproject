﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectMechanismController : MonoBehaviour {

	public CollectItemController[] collections;

	public GameObject door;
	public static float currentCollectedItem = 0;
	public float fakeCurrentCollectedItem = 0;
	// Use this for initialization
	void Start () {
		door.SetActive (true);
	}

	// Update is called once per frame
	void Update () {
		fakeCurrentCollectedItem = currentCollectedItem;
		if (currentCollectedItem == collections.Length) {
			door.SetActive (false);
		}
	}
}