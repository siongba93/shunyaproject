﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class CollectItemController : MonoBehaviour {

	public SpriteRenderer image;

	public bool collected = false;
	public bool first = false;

	void Start () {
		collected = false;
	}

	public void setTriggerFunction () {

	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Player && !first) {
			image.color = Color.red;
			collected = true;
			first = true;
			CollectMechanismController.currentCollectedItem += 1;
		}
	}

}