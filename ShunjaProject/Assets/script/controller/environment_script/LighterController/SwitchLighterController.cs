﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchLighterController : MonoBehaviour {

	#region switch
	public Animator animController;
	public SwitchState state;
	public bool canSwitch = true;
	public bool playerInside = false;
	#endregion

	#region Lighter
	public GameObject Lighter;
	public SpriteRenderer image;
	#endregion

	void Update () {
		_switchController ();
		_openLightController ();
	}

	private void _openLightController () {
		switch (state) {
			case SwitchState.Red:
				Lighter.SetActive (false);
				image.color = new Color (0.25f, 0.25f, 0.25f, 1);
				break;
			case SwitchState.Blue:
				Lighter.SetActive (true);
				image.color = Color.white;
				break;
		}
	}

	private void _switchController () {
		if (playerInside) {
			if (Input.GetKeyDown (KeyCode.W) && canSwitch) {
				//	AnimatorClipInfo[] myAnimatorClip = animController.GetCurrentAnimatorClipInfo (0);
				switch (state) {
					case SwitchState.Red:
						animController.SetBool ("blue", true);
						animController.SetBool ("red", false);
						canSwitch = false;
						break;
					case SwitchState.Blue:
						animController.SetBool ("blue", false);
						animController.SetBool ("red", true);
						canSwitch = false;
						break;
				}
			}
		}
	}

	private void _onCompleteSwitch () {
		canSwitch = true;
		switch (state) {
			case SwitchState.Red:
				state = SwitchState.Blue;
				break;
			case SwitchState.Blue:
				state = SwitchState.Red;
				break;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			playerInside = false;
		}
	}
}