﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightStarKeyController : MonoBehaviour {

	public Transform transformObject;

	public SpriteRenderer mySprite;
	public SpriteRenderer crystal;
	public GameObject crystalObject;

	public Vector2 originPos;

	public float speed;

	public LightStarKeyCheckingInsideController controller;

	public bool active = false;

	public Transform myTransform {
		get {
			return transformObject;
		}
	}

	public float mySpeed {
		get {
			return speed;
		}
	}

	public void Update () {
		controller.checking ();
	}

	public void setActive (bool state) {
		active = state;
	}

	public void setCrystal (Color color) {
		crystal.color = color;
	}

	public void setMySpriteColor (Color color) {
		mySprite.color = color;
	}

	public void setSpeed (float value) {
		speed = value;
	}

	public void setCrystalEnabled (bool state) {
		crystalObject.SetActive (state);
		active = state;
		mySprite.color = Color.grey;
		controller.setActive (false);
	}

	public void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == "LightStarDestroyer") {
			controller.setActive (false);
			active = false;
			speed = 0;
			mySprite.color = Color.grey;
			crystal.enabled = false;
			mySprite.enabled = false;
			Invoke ("_resetLightStarPosition", 0.5f);
		}
	}

	private void _resetLightStarPosition () {
		this.transform.position = originPos;
		controller.setActive (true);
		speed = 3;
		crystal.enabled = true;
		mySprite.enabled = true;
	}
}