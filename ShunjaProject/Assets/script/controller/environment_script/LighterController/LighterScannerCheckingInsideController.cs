﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighterScannerCheckingInsideController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public LigherScannerMainController controller;

	public void checking () {
		_insideChecking ();
		_checkState ();
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
	}

	private void _checkState () {
		if (WhiteInside == false) {
			controller.setLighterColor (Color.grey);
			controller.setCrystal (Color.grey);
			controller.setMovementSpeed (0);
			controller.setScannerActive (false);
			controller.setLighterActive (false);
			controller.setLighterColorAPI (Color.grey);
			return;
		}

		if (WhiteInside == true) {
			controller.setLighterColor (Color.white);
			controller.setCrystal (Color.green);
			controller.setScannerActive (true);
			controller.setMovementSpeed (30);
		}
	}
}