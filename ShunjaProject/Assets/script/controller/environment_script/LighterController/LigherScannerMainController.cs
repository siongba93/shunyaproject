﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ScannerState {
	LEFT,
	RIGHT
}

public class LigherScannerMainController : MonoBehaviour {

	public ScannerState state = ScannerState.RIGHT;
	public Transform targetRight;
	public Transform targerLeft;

	public SpriteRenderer lighter;
	public SpriteRenderer crystal;

	public GameObject scanner;

	public Transform circle;
	public float speed;

	public LighterScannerCheckingInsideController controller;
	public LighterScannerController scannerController;

	public void setLighterColor (Color color) {
		lighter.color = color;
	}

	public void setCrystal (Color color) {
		crystal.color = color;
	}

	public void setMovementSpeed (float value) {
		speed = value;
	}

	public void setScannerActive (bool state) {
		scannerController.setObjectActive (state);
	}

	public void setLighterColorAPI (Color newColor) {
		scannerController.setLighterColor (newColor);
	}

	public void setLighterActive (bool state) {
		scannerController.setLighterActive (state);
	}

	void Update () {
		controller.checking ();
		_checking ();
		_circleRotationMovement ();
	}

	private void _checking () {
		float circleZ = circle.rotation.z;
		float targetRightZ = targetRight.rotation.z;
		float targetLeftZ = targerLeft.rotation.z;

		if (circleZ == targetRightZ) {
			state = ScannerState.LEFT;
		} else if (circleZ == targetLeftZ) {
			state = ScannerState.RIGHT;
		}
	}

	private void _circleRotationMovement () {
		if (state == ScannerState.RIGHT) {
			circle.localRotation = Quaternion.RotateTowards (circle.rotation, targetRight.rotation, Time.deltaTime * speed);
		} else {
			circle.localRotation = Quaternion.RotateTowards (circle.rotation, targerLeft.rotation, Time.deltaTime * speed);
		}
	}
}