﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightStarHolderController : MonoBehaviour {

	public SourceStarController[] sourceController;
	public LightStarKeyController lightStarController;

	public GameObject door;
	public float[] distance;
	public bool[] firstAchieve;
	public float minDistance;

	public int keyHolder = 0;
	public int targetHolder = 4;

	private void _calculateLightStar (int sourceNum) {
		distance[sourceNum] = Vector2.Distance (sourceController[sourceNum].myTransform.position, lightStarController.myTransform.position);
	}

	private void _lightStarMovement (int sourceNum) {
		float speed = lightStarController.mySpeed;
		lightStarController.myTransform.position = Vector3.Lerp (lightStarController.myTransform.position, sourceController[sourceNum].myTransform.position, speed * Time.deltaTime);
	}

	void FixedUpdate () {
		_setCalculation ();
		_setDistance ();
		_lightStarMovement ();
		_checkDestination ();
		_checkDoor ();
	}

	private void _setCalculation () {
		bool source0 = sourceController[0].active;
		bool source1 = sourceController[1].active;
		bool source2 = sourceController[2].active;
		bool source3 = sourceController[3].active;
		if (source0 && sourceController[0].myCalculated == false) {
			_calculateLightStar (0);
			sourceController[0].setCalculated (true);
		}
		if (source1 == true && sourceController[1].myCalculated == false) {
			_calculateLightStar (1);
			sourceController[1].setCalculated (true);
		}
		if (source2 && sourceController[2].myCalculated == false) {
			_calculateLightStar (2);
			sourceController[2].setCalculated (true);
		}
		if (source3 == true && sourceController[3].myCalculated == false) {
			_calculateLightStar (3);
			sourceController[3].setCalculated (true);
		}
	}

	private void _setDistance () {
		bool source0 = sourceController[0].active;
		bool source1 = sourceController[1].active;
		bool source2 = sourceController[2].active;
		bool source3 = sourceController[3].active;
		if (source0 == true && source1 == true && source2 == true && source3 == true) {
			minDistance = Mathf.Min (distance);
		} else if (source0 == false && source1 == true && source2 == true && source3 == true) {
			minDistance = Mathf.Min (distance[1], distance[2], distance[3]);
		} else if (source0 == true && source1 == false && source2 == true && source3 == true) {
			minDistance = Mathf.Min (distance[0], distance[2], distance[3]);
		} else if (source0 == true && source1 == true && source2 == false && source3 == true) {
			minDistance = Mathf.Min (distance[0], distance[1], distance[3]);
		} else if (source0 == true && source1 == true && source2 == true && source3 == false) {
			minDistance = Mathf.Min (distance[0], distance[1], distance[2]);
		} else if (source0 == false && source1 == false && source2 == true && source3 == true) {
			minDistance = Mathf.Min (distance[2], distance[3]);
		} else if (source0 == false && source1 == true && source2 == false && source3 == true) {
			minDistance = Mathf.Min (distance[1], distance[3]);
		} else if (source0 == false && source1 == true && source2 == true && source3 == false) {
			minDistance = Mathf.Min (distance[1], distance[2]);
		} else if (source0 == true && source1 == false && source2 == false && source3 == true) {
			minDistance = Mathf.Min (distance[0], distance[3]);
		} else if (source0 == true && source1 == false && source2 == true && source3 == false) {
			minDistance = Mathf.Min (distance[0], distance[2]);
		} else if (source0 == true && source1 == true && source2 == false && source3 == false) {
			minDistance = Mathf.Min (distance[0], distance[1]);
		}
	}

	private void _lightStarMovement () {
		bool source0 = sourceController[0].active;
		bool source1 = sourceController[1].active;
		bool source2 = sourceController[2].active;
		bool source3 = sourceController[3].active;
		if ((minDistance == distance[0] && source0 == true) ||
			(source0 == true && source1 == false && source2 == false && source3 == false)) {
			_lightStarMovement (0);
		} else if ((minDistance == distance[1] && source1 == true) ||
			(source0 == false && source1 == true && source2 == false && source3 == false)) {
			_lightStarMovement (1);
		} else if ((minDistance == distance[2] && source2 == true) ||
			(source0 == false && source1 == false && source2 == true && source3 == false)) {
			_lightStarMovement (2);
		} else if ((minDistance == distance[3] && source3 == true) ||
			(source0 == false && source1 == false && source2 == false && source3 == true)) {
			_lightStarMovement (3);
		}
	}

	private void _checkDestination () {
		if (Vector2.Distance (lightStarController.myTransform.position, sourceController[0].myTransform.position) < 0.1 && firstAchieve[0] == false) {
			sourceController[0].setCrystalEnabled (false);
			firstAchieve[0] = true;
			keyHolder += 1;
		} else if (Vector2.Distance (lightStarController.myTransform.position, sourceController[1].myTransform.position) < 0.2 && firstAchieve[1] == false) {
			sourceController[1].setCrystalEnabled (false);
			keyHolder += 1;
			firstAchieve[1] = true;
		} else if (Vector2.Distance (lightStarController.myTransform.position, sourceController[2].myTransform.position) < 0.3 && firstAchieve[2] == false) {
			sourceController[2].setCrystalEnabled (false);
			keyHolder += 1;
			firstAchieve[2] = true;
		} else if (Vector2.Distance (lightStarController.myTransform.position, sourceController[3].myTransform.position) < 0.4 && firstAchieve[3] == false) {
			sourceController[3].setCrystalEnabled (false);
			keyHolder += 1;
			firstAchieve[3] = true;
		}
	}

	public void _checkDoor () {
		if (keyHolder == targetHolder) {
			door.SetActive (false);
			lightStarController.setCrystalEnabled (false);
		}
	}
}