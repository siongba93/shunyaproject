﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LighterScannerController : MonoBehaviour {

	public GameObject lighter;
	public GameObject scanner;

	public SpriteRenderer lighterImage;

	public void setLighterColor (Color newColor) {
		lighterImage.color = newColor;
	}

	public void setObjectActive (bool state) {
		scanner.SetActive (state);
	}

	public void setLighterActive (bool state) {
		lighter.SetActive (state);
	}

	public void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			lighter.SetActive (true);
			lighterImage.color = Color.yellow;
		}
	}

	public void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			lighter.SetActive (false);
			lighterImage.color = Color.grey;
		}
	}

}