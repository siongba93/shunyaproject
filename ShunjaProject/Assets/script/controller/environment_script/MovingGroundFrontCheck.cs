﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
public class MovingGroundFrontCheck : MonoBehaviour {

	public PlatformMoveController controller;
	// Update is called once per frame
	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareEarth) {
			controller.setSpeed (0);
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareEarth) {
			controller.setSpeed (controller.InitialSpeed);
		}
	}
}