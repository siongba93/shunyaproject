﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScreenController : MonoBehaviour
{

    public Animator enterAnim;
    public Animator startGameAnim;

    public AudioSource mainAudio;
    public AudioSource effectAudio;

    public AudioClip audioSelection;

    public GameObject LevelSelectionCanvas;
    public GameObject SquareFile;

    public bool isStartGame = false;

    public string levelSelectionName;
    // Start is called before the first frame update


    void Start()
    {
        StartCoroutine(AudioManager.FadeIn(mainAudio, 15f));
    }
    public void startGame()
    {
        enterAnim.enabled = false;
        startGameAnim.enabled = true;
    }

    public void loadStartLevel()
    {
        SquareFile.SetActive(false);
        LevelSelectionCanvas.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Return) && isStartGame == false)
        {
            AudioManager.playAudioClip(effectAudio, audioSelection, 0.1f);
            isStartGame = true;
            startGame();
        }
    }

}
