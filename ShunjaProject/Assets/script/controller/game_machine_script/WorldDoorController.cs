﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldDoorController : MonoBehaviour {

	public Transform doorDestination;
	public GameObject player;
	public bool inside = false;
	public bool moving = false;

	public PlayerMainController controller;

	public float speed = 5;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (inside) {
			if (Input.GetKeyDown (KeyCode.W)) {
				Debug.Log ("startMove");
				controller.stopAllAction ();
				moving = true;
			}
		}
		if (player != null) {
			controller = player.GetComponent<PlayerMainController> ();
			if (moving) {
				player.transform.position = Vector3.Lerp (player.transform.position, doorDestination.position, speed * Time.deltaTime);
				if (Vector3.Distance (player.transform.position, doorDestination.position) < 0.1f) {
					moving = false;
					controller.resetAllAction ();
				}
			}
		}
	}

	public void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			inside = true;
			player = collision.gameObject;
		}
	}

	public void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			inside = false;
		}
	}
}