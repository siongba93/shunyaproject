﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataBase;
using UnityEngine;

public class WorldLevelTransformController : MonoBehaviour {

	public SpriteRenderer crystal;
	public GameObject crystalObject;
	public Animator worldTransformAnim;
	public Animator crystalAnim;
	public float myTime;
	public bool firstNormalPlay = false;
	public bool firstRevertPlay = false;
	public SpriteRenderer[] spriteFile;

	public bool allPlayerInside = false;
	public WorldLevelCheckInsideController controller;

	void Start () { }

	void Update () {
		_checkPlayerInside ();
		_checkAnimationTimer ();
	}

	private void _checkAnimationTimer () {
		AnimatorStateInfo animationState = worldTransformAnim.GetCurrentAnimatorStateInfo (0);
		AnimatorClipInfo[] myAnimatorClip = worldTransformAnim.GetCurrentAnimatorClipInfo (0);

		if (myTime <= myAnimatorClip[0].clip.length) {
			myTime = myAnimatorClip[0].clip.length * animationState.normalizedTime;
		}

		if (myTime > myAnimatorClip[0].clip.length) {
			myTime = myAnimatorClip[0].clip.length;
		}
		// if (myTime < 0) {
		// 	myTime = 0;
		// }
	}

	private void _checkPlayerInside () {
		if (allPlayerInside == false) {
			controller.checking ();
		} else {
			controller.stopAllAction ();
		}
	}

	public void setSpriteRenderer (Color newColor) {
		foreach (var sprite in spriteFile) {
			sprite.color = newColor;
		}
	}

	public void setCrystalObject (bool state) {
		crystalObject.SetActive (state);
	}

	public void setAnimatorSpeed (float value) {
		worldTransformAnim.SetFloat ("speed", value);
		crystalAnim.SetFloat ("speed", value);
	}

	public void setFirstPlay (bool state) {
		firstNormalPlay = state;
		firstRevertPlay = state;
	}

	public void playNormalAnimation () {
		if (firstNormalPlay == false) {
			// worldTransformAnim.Play (0);
			worldTransformAnim.PlayInFixedTime (0, 0, myTime);
			firstNormalPlay = true;
		}
	}

	public void playRevertAnimation () {
		if (firstRevertPlay == false) {
			// worldTransformAnim.Play (0);
			worldTransformAnim.PlayInFixedTime (0, 0, -myTime);
			firstRevertPlay = true;
		}
	}

	public void setCrystalColor (Color NewColor) {
		crystal.color = NewColor;
	}

	public void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			allPlayerInside = true;
		}
	}

	public void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			allPlayerInside = false;
		}
	}

}