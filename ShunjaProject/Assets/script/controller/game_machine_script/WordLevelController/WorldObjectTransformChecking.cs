﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldObjectTransformChecking : MonoBehaviour {

	public bool playerInside = false;

	void OnTriggerStay2D (Collider2D collider) {
		if (collider.gameObject.tag == "Player") {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collider) {
		if (collider.gameObject.tag == "Player") {
			playerInside = false;
		}
	}

}