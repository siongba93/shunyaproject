﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldLevelCheckInsideController : MonoBehaviour {
	public bool WhiteInside = false;
	public bool BlackInside = false;
	public Color themeColor;
	public BlackWhiteChildBehaviour LeftController;
	public BlackWhiteChildBehaviour RightController;
	public BlackWhiteChildBehaviour UpController;
	public BlackWhiteChildBehaviour DownController;

	public WorldLevelTransformController controller;

	// Update is called once per frame
	public void checking () {
		_insideChecking ();
		_checkState ();
	}

	public void stopAllAction () {
		controller.setCrystalColor (themeColor);
		controller.setAnimatorSpeed (0);
		controller.setSpriteRenderer (themeColor);
		controller.setCrystalObject (false);
	}

	private void _insideChecking () {
		if (LeftController.WhiteInside == true && RightController.WhiteInside == true &&
			UpController.WhiteInside == true && DownController.WhiteInside == true) {
			WhiteInside = true;
		}
		if (LeftController.WhiteInside == false && RightController.WhiteInside == false &&
			UpController.WhiteInside == false && DownController.WhiteInside == false) {
			WhiteInside = false;
		}
		if (LeftController.BlackInside == true || RightController.BlackInside == true ||
			UpController.BlackInside == true || DownController.BlackInside == true) {
			BlackInside = true;
		}
		if (LeftController.BlackInside == false && RightController.BlackInside == false &&
			UpController.BlackInside == false && DownController.BlackInside == false) {
			BlackInside = false;
		}
	}

	void _checkState () {
		controller.setCrystalObject (true);
		if (WhiteInside == false && BlackInside == false) {
			controller.setCrystalColor (Color.grey);
			controller.setAnimatorSpeed (0);
			controller.setSpriteRenderer (Color.grey);
			controller.setFirstPlay (false);
			return;
		}

		if (WhiteInside == true && BlackInside == false) {
			controller.playNormalAnimation ();
			controller.setAnimatorSpeed (1);
			controller.setCrystalColor (themeColor);
			controller.setSpriteRenderer (themeColor);
		} else if (WhiteInside == false && BlackInside == true) {
			controller.playRevertAnimation ();
			controller.setAnimatorSpeed (-1);
			controller.setSpriteRenderer (themeColor);
			controller.setCrystalColor (Color.black);
		}

	}
}