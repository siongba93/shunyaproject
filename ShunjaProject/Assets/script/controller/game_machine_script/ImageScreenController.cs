﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageScreenController : MonoBehaviour
{
    public Image LevelSprite;
    // Start is called before the first frame update
    public void uiImageFadeIn()
    {
        StartCoroutine(UiFader.fadeUIElement(LevelSprite, 0, 1, 1));
    }

    public void uiImageFadeOut()
    {
        StartCoroutine(UiFader.fadeUIElement(LevelSprite, 1, 0, 1));
    }
}
