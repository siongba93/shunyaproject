﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	public Text nameText;
	public Text dialogueText;
	public Image AvatarImage;
	public Animator anim;

	public AudioSource voiceSound;
	public Button button;
	public Image buttonSprite;
	public Text buttonText;
	private Queue<string> sentences;
	// Use this for initialization
	void Start ()
	{
		sentences = new Queue<string> ();
	}

	public void StartDialogue (Dialogue dialogue)
	{
		anim.enabled = true;
		nameText.text = dialogue.name;
		AvatarImage.sprite = dialogue.Avatar;
		anim.SetBool ("IsOpen", true);
		sentences.Clear ();
		foreach (string sentence in dialogue.sentences) {
			sentences.Enqueue (sentence);
		}
		DisplayNextSentence ();
	}

	public void DisplayNextSentence ()
	{
		if (sentences.Count == 0) {
			EndDialogue ();
			return;
		}
		voiceSound.Play ();
		buttonEnabled (false);
		string sentence = sentences.Dequeue ();
		StopAllCoroutines ();
		StartCoroutine (TypeSentences (sentence));
		// dialogueText.text = sentence;
	}

	IEnumerator TypeSentences (string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray()) {
			dialogueText.text += letter;
			yield return new WaitForSeconds (0.02f);
		}
		voiceSound.Stop ();
		buttonEnabled (true);
	}

	private void EndDialogue ()
	{
		anim.SetBool ("IsOpen", false);
	}

	private void buttonEnabled (bool State)
	{
		button.interactable = State;
		buttonSprite.enabled = State;
		buttonText.enabled = State;
	}

}
