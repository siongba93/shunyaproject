﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

	public Dialogue[] dialogue;
	public int MaxNumTalk= 2;
	public int numTalk = 0;

	public void TriggerDialogue(){
		FindObjectOfType<DialogueManager> ().StartDialogue (dialogue[numTalk]);
		if (numTalk < (MaxNumTalk -1)) {
			numTalk += 1;
		}
	}
}
