﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDialogueController : MonoBehaviour {
	[Header ("Init Property")]
	public Transform Player;

	[Header ("Editable Property")]
	public Vector3 AddPos;

	void Update () {
		this.transform.position = Player.transform.position + AddPos;
	}
}
