﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using DataBase;
using UnityEngine;
using UnityEngine.UI;

// Need Add Using.UnityCursorControl
namespace UnityCursorControl
{
    public class DrawSquareController : MonoBehaviour
    {
        #region Test
        public bool fixMousePos = false;
        #endregion

        #region  Main Component Need Init

        [Header("Init Component")]
        public GameObject Player;
        public BlackWhiteDataBase overlapController;
        public SquarePrefabStoreController prefabStore;
        public CatController catController;

        #endregion

        [Header("PrefabType")]
        public PrefabType prefabType;
        public GameObject FirstPrefab;
        public GameObject SecondPrefab;
        public SquareHolderController prefabController;

        #region DataStore
        public float squarePower = 1;
        public static int NumHitPlayer;

        [Header("Data Store")]
        public GameObject FirstSquareStore;
        public GameObject SecondSquareStore;
        public int maxSquareStore = 2;

        #endregion

        #region private property

        [Header("Current Data")]
        public int Num = 0;
        public SquareBoxState CurrentSquareBox;
        private GameObject spawnSquare;
        private Rect rectangle;
        public bool CanDraw = false;
        public Vector2 _orgBoxPos = Vector2.zero;
        public Vector2 _orgPrefab = Vector2.zero;
        public Vector2 _endBoxPos = Vector2.zero;
        public Vector2 _endPrefab = Vector2.zero;
        public Vector2 _middlePrefabPos = Vector2.zero;
        public float prefabX;
        public float prefabY;
        public float CurrentPowerData;
        private bool _doubleClick = false;
        #endregion

        #region public

        [Header("Main Component")]
        public Slider powerSquareShow;
        public Text powerSquareText;
        public Camera cam;
        public GameObject RedPrefab;
        public Texture firstTexture;
        public Texture whiteTexture;
        public Texture blackTexture;
        public Texture secondTexture;
        public int prefabColor = 0;
        public Vector3 MousePos;
        public float MinScale = 0.5f;

        #endregion

        #region Distance Mouse and Player
        public float MaxDistance = 10;
        public float CurrentDistance = 0;
        private bool MouseOverDistance;

        #endregion

        [DllImport("user32.dll")]
        static extern bool SetCursorPos(float X, float Y);

        #region Main Controller

        void Start()
        {
            catController.setIdle();
            Player = GameObject.FindGameObjectWithTag("Player");
        }

        void Update()
        {
            _setPowerData();
            _setPrefabType();
            _setActiveMouse();
            //			setMouseDistance ();
        }

        public void setPlayerLocation(GameObject setPlayer)
        {
            Player = setPlayer;
        }

        public void setCurrentSquareBox(PrefabType squareNum)
        {
            prefabType = squareNum;
        }

        public void RestoreSquarePower(GameObject squareObj)
        {
            float PosX = squareObj.transform.localScale.x;
            float PosY = squareObj.transform.localScale.y;
            squarePower += (Mathf.Abs(PosX) + Mathf.Abs(PosY));
            Debug.Log("Restore Power");
        }

        private void OnGUI()
        {
            if (_orgBoxPos != Vector2.zero && _endBoxPos != Vector2.zero)
            {
                float _middleX = (_orgPrefab.x + _endPrefab.x) / 2;
                float _middleY = (_orgPrefab.y + _endPrefab.y) / 2;
                _middlePrefabPos = new Vector2(_middleX, _middleY);
                rectangle.Set(_orgBoxPos.x, Screen.height - _orgBoxPos.y,
                    _endBoxPos.x - _orgBoxPos.x, -1 * (_endBoxPos.y - _orgBoxPos.y));
                _setlocalScale();
                if (squarePower > 0 && squarePower < 1 &&
                    Mathf.Abs(prefabY) >= MinScale &&
                    Mathf.Abs(prefabX) >= MinScale)
                {
                    if (Input.GetKey(KeyCode.Mouse0))
                    {
                        CanDraw = true;
                        GUI.DrawTexture(rectangle, whiteTexture);
                    }
                    // } else if (Input.GetKey (KeyCode.Mouse1)) {
                    // 	GUI.DrawTexture (rectangle, blackTexture);
                    // }
                }
                else
                {
                    CanDraw = false;
                    GUI.DrawTexture(rectangle, secondTexture);
                }
                // if (prefabColor == 0) {
                // 	GUI.DrawTexture (rectangle, firstTexture);
                // } else if (prefabColor == 1) {
                // 	GUI.DrawTexture (rectangle, secondTexture);
                // }
            }
            // if (Event.current.type == EventType.Repaint) {
            // 	cam.Render ();
            // }
        }

        #endregion

        /** First Proceduce*/
        private void _setPowerData()
        {
            CurrentPowerData = squarePower;
            if (squarePower >= 0)
            {
                powerSquareText.color = Color.black;
                powerSquareText.text = "Value : " + squarePower + " % ";
                powerSquareShow.value = Mathf.Lerp(powerSquareShow.value, squarePower, 1 * Time.deltaTime);
            }
            else
            {
                powerSquareText.color = Color.red;
                powerSquareText.text = "Value : 0 %";
                powerSquareShow.value = 0;
            }
        }

        /** Second Proceduce*/
        private void _setPrefabType()
        {
            switch (prefabType)
            {
                case PrefabType.EarthSquare:
                    FirstPrefab = prefabStore.EarthSquare;
                    SecondPrefab = prefabStore.EarthSquare;
                    break;
                case PrefabType.FireSquare:
                    FirstPrefab = prefabStore.FireSquare;
                    SecondPrefab = prefabStore.FireSquare;
                    break;
                case PrefabType.WindSquare:
                    FirstPrefab = prefabStore.WindSquare;
                    SecondPrefab = prefabStore.WindSquare;
                    break;
                case PrefabType.WaterSquare:
                    FirstPrefab = prefabStore.WaterSquare;
                    SecondPrefab = prefabStore.WaterSquare;
                    break;
            }
        }

        /** Third Procedure*/
        private void _setActiveMouse()
        {
            //			if (Input.GetKey (KeyCode.S)) {
            //float ScreenSize = (Screen.width * Screen.height);
            // MaxDistance = 300 * (ScreenSize / 600000);

            MousePos = Input.mousePosition;
            //				if (Input.GetKeyDown (KeyCode.S)) {s
            //					_setLocalCursorPos (screenPlayer);
            //				}
            Cursor.visible = false;
            if (fixMousePos)
            {
                Vector3 screenPlayer = Camera.main.WorldToScreenPoint(Player.transform.position);
                Vector3 MouseOffset = MousePos - screenPlayer;
                _distanceCalculation();
                if (CurrentDistance <= MaxDistance)
                {
                    Cursor.visible = false;
                    // CanDraw = true;
                }
                else
                {
                    // CanDraw = false;
                    //				Cursor.visible = false;
                    _setLocalCursorPos((MouseOffset.normalized * MaxDistance) + screenPlayer);
                }
            }
            else
            {
                // CanDraw = true;
            }
            //			}
            //			else {
            //				CanDraw = false;
            //				Cursor.visible = false;
            //			}
            _disableDoubleClick();
            if (Input.GetKey(KeyCode.Mouse1) || _doubleClick == true)
            {
                // Debug.Log ("Delete Square");
                _resetAllData();
                _resetPowerData();
                _destroyStore();
                _doubleClick = true;
                return;
            }
            _leftMouseUp();
            // _rightMouseUp ();
        }

        /** Fourth Procedure */
        private void _renameSquare()
        {
            if (spawnSquare != null)
            {
                switch (prefabType)
                {
                    case PrefabType.EarthSquare:
                        spawnSquare.name = "EarthSquare01 ";
                        break;
                    case PrefabType.WaterSquare:
                        switch (CurrentSquareBox)
                        {
                            case SquareBoxState.White:
                                spawnSquare.name = "WaterSquare01 ";
                                break;
                            case SquareBoxState.Black:
                                spawnSquare.name = "WaterSquare02 ";
                                break;
                        }
                        break;
                    case PrefabType.FireSquare:
                        switch (CurrentSquareBox)
                        {
                            case SquareBoxState.White:
                                spawnSquare.name = "FireSquare01 ";
                                break;
                            case SquareBoxState.Black:
                                spawnSquare.name = "FireSquare02 ";
                                break;
                        }
                        break;
                    case PrefabType.WindSquare:
                        switch (CurrentSquareBox)
                        {
                            case SquareBoxState.White:
                                spawnSquare.name = "WindSquare01 ";
                                break;
                            case SquareBoxState.Black:
                                spawnSquare.name = "WindSquare02 ";
                                break;
                        }
                        break;
                }
            }
        }

        #region API
        /** Third Procedure API */
        private void _distanceCalculation()
        {
            Vector3 screenPlayer = Camera.main.WorldToScreenPoint(Player.transform.position);
            //Vector3 screenPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            //float ScreenSize = (Screen.width * Screen.height);
            CurrentDistance = Vector2.Distance(Input.mousePosition, screenPlayer);
        }

        /** Third Procedure API */
        private void _disableDoubleClick()
        {
            if (!Input.GetKey(KeyCode.Mouse0) && !Input.GetKey(KeyCode.Mouse1))
            {
                _doubleClick = false;
            }
        }

        /** Third Procedure API */
        private void _resetAllData()
        {
            #region reset All Data to zero
            _orgPrefab = Vector2.zero;
            _endPrefab = Vector2.zero;
            _orgBoxPos = Vector2.zero;
            _endBoxPos = Vector2.zero;
            // Debug.Log ("reset all data");
            #endregion
        }

        /** Third Procedure API */
        private void _checkPowerData()
        {
            if (squarePower > 0)
            {
                // spawnSquare.transform.parent = this.transform;
            }
            else
            {
                // _resetPowerData ();
                Destroy(spawnSquare);
                Debug.Log("SquarePower is not enough");
            }
        }

        /** Third Procedure API */
        private void _destroyLeftStore()
        {
            if (FirstSquareStore != null)
            {
                Destroy(FirstSquareStore);
                overlapController.WhiteController = null;
            }
        }

        /** Third Procedure API */
        private void _leftMouseUp()
        {
            _leftMouseDown();
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                catController.setIdle();
                if (_endBoxPos != Vector2.zero)
                {
                    // _setlocalScale ();
                    #region checkScale
                    if (CanDraw == false)
                    {
                        Debug.Log("It is too small");
                        _resetPowerData();
                        _orgBoxPos = Vector2.zero;
                        _endBoxPos = Vector2.zero;
                        return;
                    }
                    #endregion
                    _leftSpawnSquare();
                }
                _checkPowerData();
                _resetAllData();
            }
        }

        /** Third Procedure API */
        private void _leftMouseDown()
        {
            //Vector3 screenPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
            //Vector3 spawnPosition = new Vector3 (screenPosition.x, screenPosition.y, 10);
            #region BlueSquare
            if (Input.GetKey(KeyCode.Mouse0))
            {
                catController.setDrawing();
                prefabColor = 0;

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    //_orgPrefab = spawnPosition;
                    _orgBoxPos = Input.mousePosition;
                }
                else
                {
                    //_endPrefab = spawnPosition;
                    _endBoxPos = Input.mousePosition;
                }
            }
            Vector3 orgBox = Camera.main.ScreenToWorldPoint(_orgBoxPos);
            Vector3 endBox = Camera.main.ScreenToWorldPoint(_endBoxPos);
            _orgPrefab = new Vector3(orgBox.x, orgBox.y, 10);
            _endPrefab = new Vector3(endBox.x, endBox.y, 10);
            #endregion

            #region resize
            //			else {
            //				_orgBoxPos = Vector2.zero;
            //				_endBoxPos = Vector2.zero;
            //			}
            #endregion
        }

        /** Third Procedure API */
        private void _destroyStore()
        {
            if (FirstSquareStore != null)
            {
                Destroy(FirstSquareStore);
                overlapController.WhiteController = null;
            }
            if (SecondSquareStore != null)
            {
                Destroy(SecondSquareStore);
                overlapController.BlackController = null;
            }
        }

        /** Third Procedure API */
        private void _leftSpawnSquare()
        {
            spawnSquare = (GameObject)Instantiate(FirstPrefab, _middlePrefabPos, Quaternion.identity);
            spawnSquare.transform.localScale = new Vector2(prefabX, prefabY);
            prefabController = spawnSquare.GetComponent<SquareHolderController>();
            overlapController.WhiteController = spawnSquare.GetComponent<BlackWhiteSquareController>();
            StartCoroutine(checkSquareDestroyAble());
        }

        private IEnumerator checkSquareDestroyAble()
        {
            yield return new WaitForSeconds(0.1f);
            if (prefabController.canDraw)
            {
                _destroyStore();
                FirstSquareStore = spawnSquare;
                _renameSquare();
            }
        }

        /** Third Procedure API */
        private void _resetPowerData()
        {
            #region Reset Power
            squarePower = 1;
            powerSquareShow.value = Mathf.Lerp(powerSquareShow.value, 1, 1 * Time.deltaTime);
            // Debug.Log ("Restore 100 % Power");
            #endregion
        }

        /** Third Procedure API */
        private void _setLocalCursorPos(Vector3 pos)
        {
            CursorControl.SetLocalCursorPos(pos);
        }

        /** On GUI Drawing*/
        private void _setlocalScale()
        {
            #region adjust localScale
            // time 10 for make the square became bigger
            prefabX = Mathf.Abs((_orgPrefab.x - _middlePrefabPos.x) * 10);
            prefabY = Mathf.Abs((_orgPrefab.y - _middlePrefabPos.y) * 10);
            squarePower = 1 - ((Mathf.Abs(prefabX) / 20) + (Mathf.Abs(prefabY) / 20));
            #endregion
        }
        #endregion
    }
}

// private void _rightSpawnSquare () {
// 	spawnSquare = (GameObject) Instantiate (SecondPrefab, _middlePrefabPos, Quaternion.identity);
// 	spawnSquare.transform.localScale = new Vector2 (prefabX, prefabY);
// 	_renameSquare ();
// 	SecondSquareStore = spawnSquare;
// 	overlapController.BlackController = spawnSquare.GetComponent<BlackWhiteSquareController> ();
// }

// private void _rightMouseDown () {
// 	//Vector3 screenPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
// 	//Vector3 spawnPosition = new Vector3 (screenPosition.x, screenPosition.y, 10);
// 	#region BlueSquare
// 	if (Input.GetKey (KeyCode.Mouse1)) {
// 		prefabColor = 1;
// 		if (Input.GetKeyDown (KeyCode.Mouse1)) {
// 			//_orgPrefab = spawnPosition;
// 			_destroyStore ();
// 			_orgBoxPos = Input.mousePosition;
// 		} else {
// 			//_endPrefab = spawnPosition;
// 			_endBoxPos = Input.mousePosition;
// 		}
// 	}
// 	Vector3 orgBox = Camera.main.ScreenToWorldPoint (_orgBoxPos);
// 	Vector3 endBox = Camera.main.ScreenToWorldPoint (_endBoxPos);
// 	_orgPrefab = new Vector3 (orgBox.x, orgBox.y, 10);
// 	_endPrefab = new Vector3 (endBox.x, endBox.y, 10);
// 	#endregion

// 	#region resize
// 	//			else {
// 	//				_orgBoxPos = Vector2.zero;
// 	//				_endBoxPos = Vector2.zero;
// 	//			}
// 	#endregion
// }

// private void _spawnSquare () {
// 	#region store Prefab
// 	switch (CurrentSquareBox) {
// 		case SquareBoxState.HIDING:
// 			// Change Behaviour
// 			CurrentSquareBox = SquareBoxState.White;
// 			spawnSquare = (GameObject) Instantiate (SecondPrefab, _middlePrefabPos, Quaternion.identity);
// 			spawnSquare.transform.localScale = new Vector2 (prefabX, prefabY);
// 			//store object
// 			_renameSquare ();
// 			FirstSquareStore = spawnSquare;
// 			CurrentSquareBox = SquareBoxState.Black;
// 			overlapController.WhiteController = spawnSquare.GetComponent<BlackWhiteSquareController> ();
// 			break;
// 		case SquareBoxState.Black:
// 			spawnSquare = (GameObject) Instantiate (FirstPrefab, _middlePrefabPos, Quaternion.identity);
// 			spawnSquare.transform.localScale = new Vector2 (prefabX, prefabY);
// 			//store object
// 			_renameSquare ();
// 			SecondSquareStore = spawnSquare;
// 			CurrentSquareBox = SquareBoxState.White;
// 			overlapController.BlackController = spawnSquare.GetComponent<BlackWhiteSquareController> ();
// 			break;
// 		case SquareBoxState.White:
// 			spawnSquare = (GameObject) Instantiate (SecondPrefab, _middlePrefabPos, Quaternion.identity);
// 			spawnSquare.transform.localScale = new Vector2 (prefabX, prefabY);
// 			//store object
// 			_renameSquare ();
// 			FirstSquareStore = spawnSquare;
// 			CurrentSquareBox = SquareBoxState.Black;
// 			overlapController.WhiteController = spawnSquare.GetComponent<BlackWhiteSquareController> ();
// 			break;
// 		default:
// 			break;
// 	}
// 	#endregion
// }

// private void _rightMouseUp () {
// 	_rightMouseDown ();
// 	if (Input.GetKeyUp (KeyCode.Mouse1) && CanDraw == true) {
// 		if (_endBoxPos != Vector2.zero) {
// 			// _setlocalScale ();
// 			#region checkScale
// 			if (Mathf.Abs (prefabX) < MinScale || Mathf.Abs (prefabY) < MinScale) {
// 				Debug.Log ("It is too small");
// 				// _resetPowerData ();
// 				_orgBoxPos = Vector2.zero;
// 				_endBoxPos = Vector2.zero;
// 				return;
// 			}
// 			#endregion
// 			_rightSpawnSquare ();
// 		}
// 		_checkPowerData ();
// 		_resetAllData ();
// 	}
// }

// private void _destroyRightStore () {
// 	Destroy (SecondSquareStore);
// 	overlapController.BlackController = null;
// }
