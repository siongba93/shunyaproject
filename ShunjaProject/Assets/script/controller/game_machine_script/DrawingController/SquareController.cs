﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityCursorControl;
using UnityEngine;
using UnityEngine.UI;

public class SquareController : MonoBehaviour {
	public Camera MainCam;
	public GameObject circle;
	public bool CanDestory = false;
	public GameObject collisionObject;

	public Image CurrentSquareSprite;
	public Text CurrentSquareName;

	public Sprite WhiteSquare;
	public Sprite BlackSquare;

	void Update () {
		Vector3 mouseWorldPos3D = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		// Vector2 mousePos2D = new Vector2 (mouseWorldPos3D.x, mouseWorldPos3D.y);
		//Vector2 mousePos2D = new Vector2 (mouseWorldPos3D.x, mouseWorldPos3D.y);
		//Vector2 dir = Vector2.zero;
		circle.transform.position = mouseWorldPos3D;
		#region DeleteSquare
		// if (Input.GetKeyUp (KeyCode.Mouse1)) {
		// 	if (CanDestory) {
		// 		DrawSquareController.RestoreSquarePower (collisionObject);
		// 		Destroy (collisionObject.transform.gameObject);
		// 	}
		// }
		// if (CanDestory == false) {
		// 	CurrentSquareSprite.sprite = null;
		// 	CurrentSquareName.text = "";
		// }
		#endregion

		//			RaycastHit2D hit = Physics2D.Raycast (mousePos, dir);
		//			Debug.Log (hit.collider.name);
		//			if (hit.collider != null &&hit.collider.name =="BlueSquare") {
		//				switch (hit.transform.tag) {
		//				case "square":
		//					DrawSquareController.RestoreSquarePower (hit.transform.gameObject);
		//					Destroy (hit.transform.gameObject);	
		//					break;
		////				case"Enemy":
		////					DrawSquareController.RestoreSquarePower (hit.transform.parent.gameObject);
		////					Destroy (hit.transform.parent.gameObject);	
		////					break;
		//				default:
		//					Debug.Log (hit.transform.tag);
		//					break;
		//				}
		//			}
		//		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		UiController (collision.gameObject);
		if (collision.gameObject.tag == Model.SquareGround ||
			collision.gameObject.tag == Model.SquareWater ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareButton ||
			collision.gameObject.tag == Model.SquareBlackWhite ||
			collision.gameObject.tag == Model.SquareWalking ||
			collision.gameObject.tag == Model.SquareColor ||
			collision.gameObject.tag == Model.SquareCamera) {
			collisionObject = collision.gameObject;
			CanDestory = true;
		} else {
			CanDestory = false;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.SquareGround ||
			collision.gameObject.tag == Model.SquareWater ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareButton ||
			collision.gameObject.tag == Model.SquareBlackWhite ||
			collision.gameObject.tag == Model.SquareWalking ||
			collision.gameObject.tag == Model.SquareColor) {
			collisionObject = null;
			CanDestory = false;
		}
	}
	void UiController (GameObject CurrObject) {

		switch (CurrObject.name) {
			case "WhiteSquare":
				CurrentSquareSprite.sprite = WhiteSquare;
				CurrentSquareName.text = "" + CurrObject.name;
				break;
			case "BlackSquare":
				CurrentSquareSprite.sprite = BlackSquare;
				CurrentSquareName.text = "" + CurrObject.name;
				break;
			default:

				break;
		}

	}
}