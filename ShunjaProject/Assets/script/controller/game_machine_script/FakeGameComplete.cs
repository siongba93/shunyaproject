﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeGameComplete : MonoBehaviour {

	public FakeGameLevelManager controller;

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			controller.gameComplete ();
		}
	}
}