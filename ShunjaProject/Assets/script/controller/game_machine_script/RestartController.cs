﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartController : MonoBehaviour {

	public Animator anim;
	public SpriteRenderer whiteBlank;
	public string levelName;

	public bool loading = false;
	// Use this for initialization
	void Start () {
		loading = false;
	}

	void restart () {
		anim.SetBool ("restart", true);
		// anim.SetFloat ("timer", 1);
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Mouse0) && Input.GetKey (KeyCode.Mouse1) && loading == false) {
			loading = true;
			restart ();
		}
		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	private void _onCompleteLoadingAnimation () {
		SceneManager.LoadScene ("" + levelName);
	}
}