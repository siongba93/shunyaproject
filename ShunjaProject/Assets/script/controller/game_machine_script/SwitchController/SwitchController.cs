﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchController : MonoBehaviour {

	#region switch
	public Animator animController;
	public SwitchState state;
	public bool canSwitch = true;
	public bool playerInside = false;
	#endregion

	#region MovingGround
	public Transform movingGround;
	public Vector3 RedPos;
	public Vector3 BluePos;
	public float speed;
	#endregion

	void Update () {
		_switchController ();
		_movingGroundController ();
	}

	private void _movingGroundController () {
		switch (state) {
			case SwitchState.Red:
				movingGround.position = Vector3.MoveTowards (movingGround.position, RedPos, speed * Time.deltaTime);
				break;
			case SwitchState.Blue:
				movingGround.position = Vector3.MoveTowards (movingGround.position, BluePos, speed * Time.deltaTime);
				break;
		}
	}

	private void _switchController () {
		if (playerInside) {
			if (Input.GetKeyDown (KeyCode.W) && canSwitch) {
				//	AnimatorClipInfo[] myAnimatorClip = animController.GetCurrentAnimatorClipInfo (0);
				switch (state) {
					case SwitchState.Red:
						animController.SetBool ("blue", true);
						animController.SetBool ("red", false);
						canSwitch = false;
						break;
					case SwitchState.Blue:
						animController.SetBool ("blue", false);
						animController.SetBool ("red", true);
						canSwitch = false;
						break;
				}
			}
		}
	}

	private void _onCompleteSwitch () {
		canSwitch = true;
		switch (state) {
			case SwitchState.Red:
				state = SwitchState.Blue;
				break;
			case SwitchState.Blue:
				state = SwitchState.Red;
				break;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			playerInside = true;
		}
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == "Player") {
			playerInside = false;
		}
	}
}