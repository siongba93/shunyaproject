﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiFader : MonoBehaviour
{
    /// <param name="uiElement">Parameter Image UI</param>
    /// <param name="start">Parameter start value</param>
    /// <param name="end">Parameter end value</param>
    /// <param name="lerpTime">Parameter fade timer</param>
    /// <returns>Fade the Image UI to destination</returns>
    public static IEnumerator fadeUIElement(Image uiElement, float start, float end, float lerpTime)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted = Time.time - _timeStartedLerping;
        float percentageComplete = timeSinceStarted;
        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;
            float currentValue = Mathf.Lerp(start, end, percentageComplete);
            float opacity = uiElement.color.a;
            opacity = currentValue;
            uiElement.color = new Color(uiElement.color.r, uiElement.color.g, uiElement.color.b, opacity);
            yield return new WaitForEndOfFrame();
        }
        print("Done !");
    }
}
