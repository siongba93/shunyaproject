﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class LayerPlayerController : MonoBehaviour {

	public GameObject Player;
	public Rigidbody2D PlayerBody;
	public SpriteRenderer PlayerSprite;

	public Camera cam;
	public float speed;
	public bool LayerMoving = false;
	public float LayerMove = 0;
	public Vector3 CurrPlayer = Vector3.zero;
	public Vector3 PlayerPos;
	public float DestinationY;

	public float LayerNum = 1;
	public BoxCollider2D[] LayerObject01;
	public BoxCollider2D[] LayerObject02;
	public BoxCollider2D[] LayerObject03;

	void Start () {
		// Physics2D.IgnoreLayerCollision (Model.Layer01, Model.Layer01, false);
		// Physics2D.IgnoreLayerCollision (Model.Layer02, Model.Layer02, false);
		// Physics2D.IgnoreLayerCollision (Model.Layer03, Model.Layer03, false);
		// Physics2D.IgnoreLayerCollision (Model.Layer01, Model.Layer02, true);
		// Physics2D.IgnoreLayerCollision (Model.Layer01, Model.Layer03, true);
		// Physics2D.IgnoreLayerCollision (Model.Layer02, Model.Layer03, true);

		PlayerPos = Player.transform.position;
	}
	public void LayerPlayer (float MoveY) {
		if (LayerMoving == false) {
			LayerMove = MoveY;
			CurrPlayer = Player.transform.position;
			LayerMoving = true;
			DestinationY = PlayerPos.y + (4.1f * MoveY);
			PlayerBody.bodyType = RigidbodyType2D.Dynamic;
			LayerNum += MoveY;
		}
	}

	void Update () {
		if (LayerNum == 1) {
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer01, false);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer02, true);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer03, true);
			float playerOpacity = PlayerSprite.GetComponent<SpriteRenderer> ().material.color.a;
			playerOpacity = 1f;
			PlayerSprite.GetComponent<SpriteRenderer> ().material.color = new Vector4 (1, 1, 1, playerOpacity);
			// Debug.Log ("color :" + PlayerSprite.color);
			cam.orthographicSize = Mathf.Lerp (cam.orthographicSize, 15, speed * Time.deltaTime);
		} else if (LayerNum == 2) {
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer01, true);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer02, false);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer03, true);
			float playerOpacity = PlayerSprite.GetComponent<SpriteRenderer> ().material.color.a;
			playerOpacity = 0.75f;
			PlayerSprite.GetComponent<SpriteRenderer> ().material.color = new Vector4 (1, 1, 1, playerOpacity);
			// Debug.Log ("color :" + PlayerSprite.color);
			cam.orthographicSize = Mathf.Lerp (cam.orthographicSize, 12, speed * Time.deltaTime);
		} else if (LayerNum == 3) {
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer01, true);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer02, true);
			// Physics2D.IgnoreLayerCollision (Model.LayerPlayer, Model.Layer03, false);
			float playerOpacity = PlayerSprite.GetComponent<SpriteRenderer> ().material.color.a;
			playerOpacity = 0.5f;
			PlayerSprite.GetComponent<SpriteRenderer> ().material.color = new Vector4 (1, 1, 1, playerOpacity);
			// Debug.Log ("color :" + PlayerSprite.color);
			cam.orthographicSize = Mathf.Lerp (cam.orthographicSize, 9, speed * Time.deltaTime);
		}
		if (LayerMoving && CurrPlayer == Vector3.zero) {
			Debug.Log ("Moving");
			PlayerPos = Player.transform.position;
			float PosY = PlayerPos.y;
			PosY = Mathf.Lerp (PosY, DestinationY, speed * Time.deltaTime);
			Player.transform.position = new Vector3 (Player.transform.position.x, PosY);
			PlayerBody.bodyType = RigidbodyType2D.Kinematic;
		}
		if (Mathf.Abs (Player.transform.position.y - DestinationY) < 0.1f) {
			LayerMoving = false;
			PlayerBody.bodyType = RigidbodyType2D.Dynamic;
			Player.transform.position = new Vector3 (Player.transform.position.x,
				DestinationY, Player.transform.position.z);
		}
	}
}