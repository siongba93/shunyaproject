﻿using System.Collections;
using System.Collections.Generic;
using GameDataManager;
using UnityEngine;

public class FakeGameLevelManager : MonoBehaviour
{
    public GameObject GameCompleteText;
    public ImageScreenController imageScreenController;

    public void gameStart()
    {
        GameModel.gameStart = true;
        GameModel.levelComplete = false;
        GameCompleteText.SetActive(false);
        imageScreenController.uiImageFadeOut();
    }

    public void gameComplete()
    {
        GameCompleteText.SetActive(true);
    }
}
