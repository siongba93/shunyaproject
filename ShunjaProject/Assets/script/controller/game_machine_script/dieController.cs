﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dieController : MonoBehaviour {

	public Animator anim;
	public PlayerMainController mainController;

	public void playDie () {
		anim.SetBool ("reset", false);
		anim.SetBool ("die", true);
		anim.Play (0);
	}

	// Update is called once per frame
	public void dieComplete () {
		mainController.resetPlayerPosition ();
		Invoke ("resetPlayer", 0.2f);
	}

	public void resetPlayer () {
		anim.SetBool ("die", false);
		anim.SetBool ("reset", true);
	}

	public void resetComplete () {
		mainController.reset ();
	}

}