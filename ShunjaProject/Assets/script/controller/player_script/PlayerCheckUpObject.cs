﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlayerCheckUpObject : PlayerStateController {

	public PlayerMainController controller;

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.LadderTop) {
			PlayerClimbToTop = true;
			JumpForce = 0;
			VerticalUpForce = 0;
		}

	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.LadderTop) {
			PlayerClimbToTop = false;
			JumpForce = InitialJumpForce;
			VerticalUpForce = InitialVerticalForce;
		}
	}
}