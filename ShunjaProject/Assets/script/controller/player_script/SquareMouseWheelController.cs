﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityCursorControl;
using UnityEngine;
using UnityEngine.UI;

public class SquareMouseWheelController : MonoBehaviour {

	public int squareNum;
	public DrawSquareController drawController;
	public Image squareImage;

	public Sprite waterSquare;
	public Sprite windSquare;
	public Sprite fireSquare;
	public Sprite earthSquare;

	// Use this for initialization
	void Start () {
		squareNum = 0;
	}

	// Update is called once per frame
	void Update () {
		squareNumCalculation ();
		imageSprite ();
	}

	public void squareNumCalculation () {
		var mouseNum = Input.GetAxis ("Mouse ScrollWheel");
		if (mouseNum > 0f) {
			// scroll up
			if (squareNum >= 3) {
				squareNum = 0;
			} else {
				squareNum += 1;
			}
		} else if (mouseNum < 0f) {
			// scroll down
			if (squareNum <= 0) {
				squareNum = 3;
			} else {
				squareNum -= 1;
			}
		}
	}

	public void imageSprite () {
		switch (squareNum) {
			case 0:
				squareImage.sprite = earthSquare;
				drawController.setCurrentSquareBox (PrefabType.EarthSquare);
				break;
			case 1:
				squareImage.sprite = waterSquare;
				drawController.setCurrentSquareBox (PrefabType.WaterSquare);
				break;
			case 2:
				squareImage.sprite = windSquare;
				drawController.setCurrentSquareBox (PrefabType.WindSquare);
				break;
			case 3:
				squareImage.sprite = fireSquare;
				drawController.setCurrentSquareBox (PrefabType.FireSquare);
				break;
			default:
				break;
		}
	}

}