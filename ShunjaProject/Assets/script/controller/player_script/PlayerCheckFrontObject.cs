﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlayerCheckFrontObject : PlayerStateController {
	public string Name;
	public bool stillInside;
	public Collider2D other;

	void Update () {
		if (stillInside && other != null) {
			HorizontalForce = 0;
		} else if (other == null) {
			HorizontalForce = HorizontalForceStatic;
			Name = null;
			HorizontalForceLeft = -InitialHorizontalForce;
			HorizontalForceRight = InitialHorizontalForce;
			PlayerCanMoveRight = true;
			PlayerCanMoveLeft = true;
		}
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareEarth) {
			if (!PlayerSloping) {
				//		PlayerCanMove = false;
				other = collision;
				stillInside = true;
				if (PlayerDirection == 1) {
					HorizontalForceRight = 0;
					HorizontalForceLeft = -InitialHorizontalForce;
					PlayerCanMoveRight = false;
				} else if (PlayerDirection == -1) {
					HorizontalForceLeft = 0;
					HorizontalForceRight = InitialHorizontalForce;
					PlayerCanMoveLeft = false;
				}
				Name = collision.gameObject.name;
			}
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareEarth) {
			if (!PlayerSloping) {
				//		PlayerCanMove = false;
				other = collision;
				stillInside = true;
				if (PlayerDirection == 1) {
					HorizontalForceRight = 0;
					HorizontalForceLeft = -InitialHorizontalForce;
					PlayerCanMoveRight = false;
				} else if (PlayerDirection == -1) {
					HorizontalForceLeft = 0;
					HorizontalForceRight = InitialHorizontalForce;
					PlayerCanMoveLeft = false;
				}
				Name = collision.gameObject.name;
			}
		}
		// } else {
		// 	HorizontalForceLeft = -InitialHorizontalForce;
		// 	HorizontalForceRight = InitialHorizontalForce;
		// 	PlayerCanMoveRight = true;
		// 	PlayerCanMoveLeft = true;
		// 	stillInside = false;
		// 	Name = null;
		//  }
		// if (collision == null) {
		// 	HorizontalForceLeft = -InitialHorizontalForce;
		// 	HorizontalForceRight = InitialHorizontalForce;
		// 	PlayerCanMoveRight = true;
		// 	PlayerCanMoveLeft = true;
		// 	stillInside = false;
		// 	Name = null;
		// }
	}

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareEarth) {
			HorizontalForceLeft = -InitialHorizontalForce;
			HorizontalForceRight = InitialHorizontalForce;
			stillInside = false;
			PlayerCanMoveRight = true;
			PlayerCanMoveLeft = true;
			Name = null;
		}
	}
}