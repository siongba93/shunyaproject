﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityCursorControl;
using UnityEngine;
public enum CurrentState {
	PlayerStanding = 0,
	PlayerRunning = 1,
	PlayerJumping = 2,
	PlayerFalling = 3,
	PlayerSquating
}

public enum PlayerCondition {
	Normal,
	OnWater,
	OnWind,
	OnFire,
	OnClimb,
	OnSpace,
	OnSafe,
	OnDanger,
}

public enum CurrentPlayer {
	Player,
	Player02
}

public class PlayerStateController : MonoBehaviour {

	#region CurrentState 
	public static CurrentState _playerCurrentState = 0;
	public static Vector3 ladderPosition;
	#endregion

	#region PlayerCondition
	public static PlayerCondition PlayerStatus;
	public static bool PlayerIsGround;
	public static bool PlayerCanMoveLeft = true;
	public static bool PlayerCanMoveRight = true;
	public static bool PlayerCanClimb = false;
	public static bool PlayerOnClimbWalking = false;
	public static bool PlayerClimbing = false;
	public static bool PlayerClimbToTop = false;
	public static bool PlayerClimbToBottom = false;
	public static bool PlayerMoving = false;
	public static bool Playerjumping = false;
	public static bool Playerkneeling;
	public static bool PlayerFalling;
	public static bool PlayerSquating;
	public static bool PlayerLookUp;
	public static bool PlayerSloping;
	public static bool PlayerHoldingKey = false;
	public static bool PlayerCanHoldKey = false;
	public static int PlayerDirection = 1;
	public static float HorizontalForce = 5;
	public static float HorizontalForceLeft = -1;
	public static float HorizontalForceRight = 1;
	public static float InitialHorizontalForce = 1;
	public static float HorizontalForceStatic = 8f;
	public static float VerticalUpForce = 0.5f;
	public static float VerticalDownForce = -0.5f;
	public static float InitialVerticalForce = 0.5f;
	public static float JumpForce = 3;
	public static float InitialJumpForce = 3;
	public static float JumpWaterForce = 3;
	public static float WaterHealPoint = 10;
	public static float FireHealPoint = 3;
	public static float InitialWaterHealPoint = 10;
	public static float InitialFireHealPoint = 3;
	public static bool CameraCanMove = true;
	public static bool TeleportPlayer;
	#endregion

}