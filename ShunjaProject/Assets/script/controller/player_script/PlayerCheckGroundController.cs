﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlayerCheckGroundController : PlayerStateController {
	public GameObject Player;
	public bool first = false;
	public string CollisionName;
	public bool stillGround;
	public Collider2D other;

	public PlayerMovementController playerMovementController;

	void Update () {
		if (stillGround && other != null) {
			PlayerIsGround = true;
		} else if (other == null || other.gameObject != null) {
			PlayerIsGround = false;
			stillGround = false;
		}
	}

	void OnTriggerEnter2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareWalkingChild ||
			collision.gameObject.tag == Model.SquareEarth ||
			collision.gameObject.tag == Model.Key) {
			// Player.transform.parent = collision.transform;
			PlayerIsGround = true;
			stillGround = true;
			other = collision;
		}
		if (collision.gameObject.tag == Model.LadderBottom) {
			PlayerClimbToBottom = true;
		}
		if (collision.gameObject.tag == Model.EnemyUp) {
			Debug.Log ("EnemyJump");
			playerMovementController.stepOnEnemy ();
		}
		if (collision.gameObject.tag == Model.JumpGround ||
			collision.gameObject.tag == Model.Bubble) {
			Debug.Log ("EnemyJump");
			playerMovementController.stepOnJumpGround ();
		}

		if (collision.gameObject.tag == Model.SquareGround) {
			Debug.Log ("OnMovingGround");
			PlayerIsGround = true;
		}
	}

	void OnTriggerStay2D (Collider2D collision) {
		if (collision.gameObject.activeInHierarchy == true) {
			if (collision.gameObject.tag == Model.Ground ||
				collision.gameObject.tag == Model.SquareGround ||
				collision.gameObject.tag == Model.SquareBox ||
				collision.gameObject.tag == Model.SquareWalkingChild ||
				collision.gameObject.tag == Model.SquareEarth ||
				collision.gameObject.tag == Model.Key) {
				PlayerIsGround = true;
				stillGround = true;
				other = collision;
			}
		} else {
			PlayerIsGround = false;
			stillGround = false;
		}
		if (collision.gameObject.tag == Model.LadderBottom) {
			PlayerClimbToBottom = true;
		}
	}
	//

	void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareGround ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareEarth ||
			collision.gameObject.tag == Model.Key) {
			PlayerIsGround = false;
			// Player.transform.parent = null;
			stillGround = false;
		}
		if (collision.gameObject.tag == Model.LadderBottom) {
			PlayerClimbToBottom = false;
		}
		if (collision.gameObject.tag == Model.SquareGround) { }
	}
}