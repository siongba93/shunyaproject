﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSquareStatusController : PlayerStateController {

	[Header ("PlayerStatus")]
	private bool first = false;
	public bool stillInside = false;
	public PlayerCondition playerConditionText;

	[Header ("PlayerWaterStatus")]
	public bool insideWater = false;
	public bool insideFire = false;
	public Color outsideColor;

	public Collider2D other;
	public string gameObjectTagName;
	public Rigidbody2D playerBody;
	public Text waterHealPointText;
	public Text fireHealPointText;
	public PlayerMainController playerMainController;
	public PlayerMovementController playerMovementController;

	void Start () {
		insideWater = false;
		insideFire = false;
		waterHealPointText.text = "" + WaterHealPoint;
		fireHealPointText.text = "" + FireHealPoint;
		first = false;
	}

	void Update () {
		if (!insideWater) {
			waterHealPointText.color = Color.Lerp (waterHealPointText.color, outsideColor, 1f * Time.deltaTime);
		} else if (insideWater) {
			if (WaterHealPoint <= 3 && WaterHealPoint > 0) {
				Debug.Log ("HP less than 3");
				waterHealPointText.color = Color.red;
			} else if (WaterHealPoint <= 0) {
				Debug.Log ("HP less than 0");
				CancelInvoke ("minusWaterHp");
				insideWater = false;
				playerMainController.playerDie ();
				return;
			} else {
				waterHealPointText.color = Color.yellow;
			}
		}

		if (!insideFire) {
			fireHealPointText.color = Color.Lerp (fireHealPointText.color, outsideColor, 1f * Time.deltaTime);
		} else if (insideFire) {
			if (FireHealPoint > 0) {
				Debug.Log ("HP less than 3");
				fireHealPointText.color = Color.green;
			} else if (FireHealPoint <= 0) {
				Debug.Log ("HP less than 0");
				CancelInvoke ("minusFireHp");
				insideFire = false;
				playerMainController.playerDie ();
				return;
			} else {
				fireHealPointText.color = Color.yellow;
			}
		}

		waterHealPointText.text = "" + WaterHealPoint;
		fireHealPointText.text = "" + FireHealPoint;
		playerConditionText = PlayerStatus;
		// if (stillInside && other != null) {
		// 	switch (gameObjectTagName) {
		// 		case "SquareWater":
		// 			// waterHealPointText.color = Color.Lerp (outsideWaterColor, insideWaterColor, 0.01f * Time.deltaTime);
		// 			if (first == false) {
		// 				PlayerStatus = PlayerCondition.OnWater;
		// 				waterHealPointCalculation ();
		// 				first = true;
		// 			}
		// 			break;
		// 		case "SquareSpace":
		// 			if (first == false) {
		// 				playerBody.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		// 				PlayerStatus = PlayerCondition.OnSpace;
		// 				first = true;
		// 			}
		// 			break;
		// 		case "SquareWalking":
		// 			if (first == false) {
		// 				stillInside = true;
		// 				first = true;
		// 			}
		// 			break;
		// 	}
		// } else {
		// 	PlayerStatus = PlayerCondition.Normal;
		// 	first = false;
		// }
	}

	public void OnTriggerEnter2D (Collider2D collision) {
		gameObjectTagName = collision.gameObject.tag;
		other = collision;
		switch (gameObjectTagName) {
			case "SquareWater":
				// if (first == false) {
				// 	playerBody.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
				// 	PlayerStatus = PlayerCondition.OnWater;
				// 	waterHealPointCalculation ();
				// 	first = true;
				// }
				break;
			case "SquareSpace":
				if (first == false) {
					playerBody.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
					PlayerStatus = PlayerCondition.OnSpace;
					stillInside = true;
					first = true;
				}
				break;
			case "SquareWalking":
				if (first == false) {
					stillInside = true;
					first = true;
				}
				break;
			case "Ladder":
				if (first == false) {
					PlayerCanClimb = true;
					ladderPosition = collision.gameObject.transform.position;
					stillInside = true;
				}
				break;
				// case "ground":
				// 	playerMainController.playerDie ();
				// 	break;
		}
	}

	public void OnTriggerStay2D (Collider2D collision) {
		gameObjectTagName = collision.gameObject.tag;
		other = collision;
		switch (gameObjectTagName) {
			case "SquareWater":
				if (first == false) {
					PlayerStatus = PlayerCondition.OnWater;
					waterHealPointCalculation ();
					stillInside = true;
					insideWater = true;
					first = true;
				}

				break;
			case "SquareWind":
				if (first == false) {
					PlayerStatus = PlayerCondition.OnWind;
					stillInside = true;
					first = true;
				}

				break;
			case "SquareFire":
				if (first == false) {
					PlayerStatus = PlayerCondition.OnFire;
					fireHealPointCalculation ();
					stillInside = true;
					insideFire = true;
					first = true;
				}

				break;
			case "SquareSpace":
				if (first == false) {
					playerBody.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
					PlayerStatus = PlayerCondition.OnSpace;
					stillInside = true;
					first = true;
				}
				break;
			case "SquareWalking":
				if (first == false) {
					stillInside = true;
					first = true;
				}
				break;
			case "Ladder":
				PlayerCanClimb = true;
				ladderPosition = collision.gameObject.transform.position;
				stillInside = true;
				break;
				// case "ground":
				// 	playerMainController.playerDie ();
				// 	break;
		}
	}

	public void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.tag == Model.Ground ||
			collision.gameObject.tag == Model.SquareGround ||
			collision.gameObject.tag == Model.SquareBox ||
			collision.gameObject.tag == Model.SquareWalking ||
			collision.gameObject.tag == Model.Ladder ||
			collision.gameObject.tag == Model.SquareWater ||
			collision.gameObject.tag == Model.SquareWind ||
			collision.gameObject.tag == Model.SquareFire) {
			stillInside = false;
			first = false;
			PlayerStatus = PlayerCondition.Normal;
		}

		if (collision.gameObject.tag == Model.SquareWater) {
			stillInside = false;
			first = false;
			insideWater = false;
			// playerMovementController.exitWaterJump ();
			playerMainController.resetWaterHealPoint ();
			CancelInvoke ("minusWaterHp");
		}

		if (collision.gameObject.tag == Model.SquareFire) {
			insideFire = false;
			stillInside = false;
			first = false;
			playerMainController.resetFireHealPoint ();
			CancelInvoke ("minusFireHp");
		}

		if (collision.gameObject.tag == Model.Ladder) {
			PlayerCanClimb = false;
		}

	}

	private void waterHealPointCalculation () {
		Invoke ("minusWaterHp", 1);
	}

	private void fireHealPointCalculation () {
		Invoke ("minusFireHp", 1);
	}

	public void minusWaterHp () {
		WaterHealPoint -= 1;
		Invoke ("minusWaterHp", 1);
	}

	public void minusFireHp () {
		FireHealPoint -= 1;
		Invoke ("minusFireHp", 1);
	}

}