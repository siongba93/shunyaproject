﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;
using GameDataManager;

public class PlayerMovementController : PlayerStateController
{
    #region public

    [Header("CameraPosition")]
    public Transform CameraPos;

    [Header("Basic Property")]
    public Animator PlayerAnim;
    public GameObject Player;
    public float finalDistance = 0;
    public float distToGround = 2;
    public float minusGround = 0;
    public Vector2 PlayerVelocity;
    public float CurrentAngle;

    [Header("Slope Property")]
    public LayerMask GroundMask;
    public float slopeFriction;
    public float angle = 0;
    public Rigidbody2D PlayerBody;
    public Vector2 SlopeWalk = new Vector2(1, 0);
    public RaycastHit2D hit;
    public float hitX;
    public float force;

    [Header("Jump Property")]
    // public float fallMultiplier = 2.5f;
    // public float lowJumpMutiplier = 2f;
    // public float highMultiplier = 1.5f;
    public float jumpTimeCounter = 0;
    public float jumpTime = 3;
    public float fallTimer = 0;
    public float maxFallTimer = 3;
    public float maxFallVelocity = -10f;
    public float maxJumpVelocity = 30;

    [Header("Child Property")]
    public GameObject CheckGroundObject;
    public SpriteRenderer PlayerSprite;
    public Transform playerSpriteTransform;
    #endregion

    [Header("Controller")]
    public PlayerCheckGroundController _checkDown;
    public PlayerMainController mainController;
    #region private

    [Header("WindSquare")]
    public Vector2 windForce = new Vector2(0, 0);

    [Header("WaterSquare")]
    public float waterSpeed = 0;

    #endregion

    #region MAIN CONTROLLER
    void Start()
    {
        CameraPos = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }
    //Set a Movement of Player
    void Update()
    {
        if (GameModel.gameStart == true)
        {
            PlayerVelocity = PlayerBody.velocity;
            setPlayerMovement();
        }
    }

    void LateUpdate()
    {
        GravityScaleControl();
    }

    public void setPlayerMovement()
    {
        _fixedRotation();
        //	_fixedScale ();
        _velocityControl();
        //		IsGround ();
        _setWaterSpeed();
        _playerHorizontalMove();
        _setPlayerAnim();
        _playerSquatingController();
        _playerLookUpController();
        _climbingControl();
    }

    public void NormalizeSlope()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
            case PlayerCondition.OnSafe:
                if (angle > 25)
                {
                    minusGround = (0.5f / (angle / 45));
                    CurrentAngle = angle;
                    PlayerSloping = true;
                }
                else if (angle <= 25)
                {
                    PlayerSloping = false;
                    minusGround = 1.3f;
                }

                finalDistance = Mathf.Abs(distToGround - minusGround);
                hit = Physics2D.Raycast(transform.position, -Vector2.up, finalDistance, GroundMask);
                angle = Mathf.Abs(Mathf.Atan2(hit.normal.x, hit.normal.y) * Mathf.Rad2Deg);

                SetSlopeWalk();
                // Attempt vertical normalization
                if (PlayerIsGround)
                {
                    if (hit.collider != null && Mathf.Abs(hit.normal.x) > 0.4f && PlayerMoving == false && Playerjumping == false)
                    {
                        PlayerBody.gravityScale += hit.normal.x * slopeFriction;
                        PlayerBody.velocity -= new Vector2(hit.normal.x * slopeFriction, 0);
                    }
                    else
                    {
                        PlayerBody.velocity = new Vector2(0, PlayerBody.velocity.y);
                    }
                }
                break;
        }
    }

    public void stepOnEnemy()
    {
        PlayerBody.velocity = new Vector2(0, JumpForce);
    }

    public void stepOnJumpGround()
    {
        PlayerBody.velocity = new Vector2(0, JumpForce * 5);
    }

    public void windSquareAddForce()
    {
        windForce = new Vector2(SquareData.windHorizontalForce, SquareData.windVerticalForce);
        PlayerBody.AddForce(
            windForce
        );
    }

    public void exitWaterJump()
    {
        PlayerBody.velocity = new Vector2(0, JumpForce * 0.75f);
    }

    #endregion

    #region First API :

    //set player horizontal movement control
    private void _playerHorizontalMove()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        Transform playerTransform = Player.transform;
        if (horizontalInput != 0 && PlayerSquating == false)
        {
            if (horizontalInput > 0 && PlayerCanMoveRight)
            {
                _jump();
                PlayerMoving = true;
                PlayerDirection = 1;
                playerTransform.Translate(SlopeWalk * HorizontalForceRight * waterSpeed * Time.deltaTime);
                return;
            }
            else if (horizontalInput > 0 && !PlayerCanMoveRight)
            {
                PlayerMoving = false;
            }
            else if (horizontalInput < 0 && PlayerCanMoveLeft)
            {
                _jump();
                PlayerMoving = true;
                PlayerDirection = -1;
                playerTransform.Translate(SlopeWalk * HorizontalForceLeft * waterSpeed * Time.deltaTime);
                return;
            }
            else if (horizontalInput < 0 && !PlayerCanMoveLeft)
            {
                PlayerMoving = false;
            }
        }
        else if (horizontalInput == 0 && !PlayerOnClimbWalking)
        {
            PlayerMoving = false;
        }
        _jump();
    }

    //set player jump control
    private void _jump()
    {
        if (PlayerSquating == false)
        {
            switch (PlayerStatus)
            {
                case PlayerCondition.Normal:
                case PlayerCondition.OnFire:
                    if (PlayerIsGround && Input.GetKeyDown(KeyCode.Space))
                    {
                        Playerjumping = true;
                        PlayerBody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
                        //Player.transform.Translate(new Vector2(0,JumpForce)*Time.deltaTime);
                        // playerBody.AddForce (new Vector2 (0, JumpForce), ForceMode2D.Impulse);
                    }
                    break;
                case PlayerCondition.OnWater:
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        //Player.transform.Translate(new Vector2(0,JumpForce)*Time.deltaTime);
                        PlayerBody.AddForce(Vector2.up * JumpWaterForce, ForceMode2D.Impulse);
                    }
                    break;

                case PlayerCondition.OnSpace:
                    if (Input.GetKey(KeyCode.Space))
                    {
                        transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z - 1);
                    }
                    break;
            }
        }
    }

    private void _climbingControl()
    {
        Transform playerTransform = Player.transform;
        // Inside Ladder

        // EscapeFromClimbing
        if (PlayerCanClimb)
        {
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetKeyDown(KeyCode.Space))
            {
                PlayerClimbing = false;
                PlayerStatus = PlayerCondition.Normal;
            }

            // OnClimb StopState
            if (PlayerClimbing && Input.GetAxisRaw("Vertical") == 0)
            {
                _setPlayerAnimSpeed(0);
            }
            //  ClimbUp
            else if (Input.GetKey(KeyCode.W) && Input.GetAxisRaw("Horizontal") == 0)
            {
                if (PlayerClimbToTop)
                {
                    _setPlayerAnimSpeed(0);
                }
                else
                {
                    _setPlayerAnimSpeed(1);
                }
                if (Mathf.Abs(playerTransform.position.x - ladderPosition.x) < 0.02f)
                {
                    PlayerMoving = false;
                    PlayerClimbing = true;
                    PlayerOnClimbWalking = false;
                    PlayerStatus = PlayerCondition.OnClimb;
                    playerTransform.Translate(new Vector2(0, VerticalUpForce) * Time.deltaTime);
                }
                else if (playerTransform.position.x > ladderPosition.x)
                {
                    PlayerDirection = -1;
                    PlayerOnClimbWalking = true;
                    PlayerMoving = true;
                    playerTransform.Translate(SlopeWalk * HorizontalForceLeft * Time.deltaTime);
                }
                else if (playerTransform.position.x < ladderPosition.x)
                {
                    PlayerDirection = 1;
                    PlayerOnClimbWalking = true;
                    PlayerMoving = true;
                    playerTransform.Translate(SlopeWalk * HorizontalForceRight * Time.deltaTime);
                }
                // ClimbDown
            }
            else if (Input.GetKey(KeyCode.S) && Input.GetAxisRaw("Horizontal") == 0)
            {
                if (PlayerClimbToBottom)
                {
                    _setPlayerAnimSpeed(0);
                }
                else
                {
                    _setPlayerAnimSpeed(1);
                    if (Mathf.Abs(playerTransform.position.x - ladderPosition.x) < 0.02f)
                    {
                        playerTransform.Translate(new Vector2(0, VerticalDownForce) * Time.deltaTime);
                        PlayerClimbing = true;
                        PlayerMoving = false;
                        PlayerOnClimbWalking = false;
                        PlayerStatus = PlayerCondition.OnClimb;
                    }
                    else if (playerTransform.position.x > ladderPosition.x)
                    {
                        PlayerDirection = -1;
                        PlayerMoving = true;
                        PlayerOnClimbWalking = true;
                        playerTransform.Translate(SlopeWalk * HorizontalForceLeft * Time.deltaTime);
                    }
                    else if (playerTransform.position.x < ladderPosition.x)
                    {
                        PlayerDirection = 1;
                        PlayerMoving = true;
                        PlayerOnClimbWalking = true;
                        playerTransform.Translate(SlopeWalk * HorizontalForceRight * Time.deltaTime);
                    }

                }
                // Outside Ladder
            }
            else
            {
                _setPlayerAnimSpeed(1);
                PlayerOnClimbWalking = false;
                PlayerStatus = PlayerCondition.Normal;
                PlayerClimbing = false;
            }
        }
    }

    private void _setPlayerAnimSpeed(float speed)
    {
        PlayerAnim.speed = speed;
    }

    private void _setWaterSpeed()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
            case PlayerCondition.OnWind:
            case PlayerCondition.OnFire:
                waterSpeed = 1;
                break;
            case PlayerCondition.OnWater:
                waterSpeed = 0.5f;
                break;
        }
    }

    private void _velocityControl()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
                maxJumpVelocity = 8;
                maxFallVelocity = -5;
                if (PlayerVelocity.y < maxFallVelocity)
                {
                    // mainController.playerDie ();
                    // Debug.Log ("Player Die");
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxFallVelocity);
                }
                else if (PlayerVelocity.y > maxJumpVelocity)
                {
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxJumpVelocity);
                }
                if (PlayerBody.velocity.y == maxFallVelocity)
                {
                    fallTimer += 1 * Time.deltaTime;
                    if (fallTimer > maxFallTimer)
                    {
                        // mainController.playerDie ();
                        fallTimer = 0;
                    }
                }
                else
                {
                    fallTimer = 0;
                }
                break;
            case PlayerCondition.OnWater:
                maxJumpVelocity = 2;
                maxFallVelocity = -1;
                if (PlayerVelocity.y < maxFallVelocity)
                {
                    // mainController.playerDie ();
                    // Debug.Log ("Player Die");
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxFallVelocity);
                }
                else if (PlayerVelocity.y > maxJumpVelocity)
                {
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxJumpVelocity);
                }
                break;
            case PlayerCondition.OnWind:
                maxJumpVelocity = 8;
                maxFallVelocity = -5;
                if (PlayerVelocity.y < maxFallVelocity)
                {
                    // mainController.playerDie ();
                    // Debug.Log ("Player Die");
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxFallVelocity);
                }
                else if (PlayerVelocity.y > maxJumpVelocity)
                {
                    PlayerBody.velocity = new Vector2(PlayerBody.velocity.x, maxJumpVelocity);
                }
                break;
        }
    }

    private void _fixedRotation()
    {
        this.transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }

    private void _fixedScale()
    {
        this.transform.localScale = new Vector3(10f, 10f, 1f);
    }

    //set player animation
    private void _setPlayerAnim()
    {
        PlayerAnim.SetBool("Climb", PlayerClimbing);
        PlayerAnim.SetBool("IsGround", PlayerIsGround);
        PlayerAnim.SetBool("Jump", !PlayerIsGround);
        PlayerAnim.SetBool("Squat", PlayerSquating);
        PlayerAnim.SetBool("LookUp", PlayerLookUp);
        PlayerAnim.SetBool("Move", PlayerMoving);
    }

    //get player Sprite Direction
    private void getPlayerDirection()
    {
        Transform playerTransform = Player.transform;
        Vector3 currRot = playerTransform.eulerAngles;
        currRot.y = 0;
        playerTransform.eulerAngles = currRot;
    }

    private void _playerSquatingController()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
            case PlayerCondition.OnWater:
            case PlayerCondition.OnFire:
                _setPlayerDirection();
                if ((Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) &&
                    (!PlayerCanClimb || PlayerClimbToBottom))
                {
                    PlayerSquating = true;
                }
                else
                {
                    PlayerSquating = false;
                }
                break;
            case PlayerCondition.OnSpace:
                if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                {
                    transform.Rotate(transform.rotation.x, transform.rotation.y, transform.rotation.z + 1);
                }
                break;
            case PlayerCondition.OnClimb:
                _setPlayerDirection();
                if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
                {
                    PlayerSquating = true;
                }
                else
                {
                    PlayerSquating = false;
                }
                break;
            case PlayerCondition.OnWind:
                _setPlayerDirection();
                break;
        }
    }

    private void _playerLookUpController()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
                if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.Space))
                {
                    PlayerLookUp = true;
                }
                else
                {
                    PlayerLookUp = false;
                }
                break;
        }
    }

    //	public void IsGround ()
    //	{
    //		Debug.DrawRay (transform.position, -Vector2.up * finalDistance, Color.red);
    //		PlayerIsGround = Physics2D.Raycast (Player.transform.position, -Vector2.up, finalDistance, GroundMask);
    //	}

    #endregion

    #region second API :

    //set player Sprite Direction
    private void _setPlayerDirection()
    {
        Transform playerTransform = Player.transform;
        if (PlayerDirection == 1)
        {
            playerSpriteTransform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        else if (PlayerDirection == -1)
        {
            playerSpriteTransform.localRotation = Quaternion.Euler(0, -180, 0);
        }
    }

    private void GravityScaleControl()
    {
        switch (PlayerStatus)
        {
            case PlayerCondition.Normal:
                PlayerBody.gravityScale = 1f;
                // //	PlayerBody.constraints = RigidbodyConstraints2D.FreezeRotation;
                // if (PlayerBody.velocity.y < 0) {
                // 	PlayerBody.gravityScale = fallMultiplier;
                // } else if (PlayerBody.velocity.y > 0 && Input.GetKey (KeyCode.Space)) {
                // 	PlayerBody.gravityScale = lowJumpMutiplier;
                // } else {
                // 	PlayerBody.gravityScale = highMultiplier;
                // }
                NormalizeSlope();
                break;
            case PlayerCondition.OnWater:
            case PlayerCondition.OnFire:
            case PlayerCondition.OnWind:
                //	PlayerBody.constraints = RigidbodyConstraints2D.FreezeRotation;
                PlayerBody.gravityScale = 0.3f;
                break;
            case PlayerCondition.OnSpace:
                PlayerBody.gravityScale = 0f;
                //	PlayerBody.constraints = RigidbodyConstraints2D.None;
                break;
            case PlayerCondition.OnClimb:
                if (PlayerClimbing)
                {
                    float playerBodyVelocityY = PlayerBody.velocity.y;
                    if (Input.GetAxisRaw("Vertical") != 0 || playerBodyVelocityY < 0)
                        PlayerBody.velocity = new Vector2(0, 0);
                }
                PlayerBody.gravityScale = 0;
                break;
        }
    }

    private void SetSlopeWalk()
    {
        hitX = Mathf.Abs(hit.normal.x);
        force = 1f - (angle / 90);
        float SetAngle = angle / 90;
        if (angle > 10)
        {
            if (PlayerDirection == -1 && hit.normal.x > 0)
            {
                SlopeWalk = new Vector2(force, force * SetAngle);
            }
            else if (PlayerDirection == 1 && hit.normal.x > 0)
            {
                SlopeWalk = new Vector2(force, -force * 1.5f);
            }
            else if (PlayerDirection == -1 && hit.normal.x < 0)
            {
                SlopeWalk = new Vector2(force, -force * 1.5f);
            }
            else if (PlayerDirection == 1 && hit.normal.x < 0)
            {
                SlopeWalk = new Vector2(force, force * SetAngle);
            }
        }
        else
        {
            SlopeWalk = Vector2.right;
        }
    }

    public void fixedRigidbody()
    {
        PlayerBody.velocity = Vector2.zero;
        PlayerBody.bodyType = RigidbodyType2D.Kinematic;
        PlayerBody.simulated = false;
        PlayerBody.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    public void resetRigidbody()
    {
        PlayerBody.velocity = Vector2.zero;
        PlayerBody.bodyType = RigidbodyType2D.Dynamic;
        PlayerBody.simulated = true;
        PlayerBody.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    #endregion
}
