﻿using System.Collections;
using System.Collections.Generic;
using DataBase;
using UnityEngine;

public class PlayerCollisionController : PlayerStateController {

	public PlayerMainController controller;
	public PlayerMovementController movementController;

	void OnTriggerEnter2D (Collider2D other) {

	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == Model.SquareWind) {
			movementController.windSquareAddForce ();
		}
	}

	void OnCollisionEnter2D (Collision2D other) {
		if (other.gameObject.tag == Model.Trap || other.gameObject.tag == Model.Enemy) {
			controller.playerDie ();
		}
	}

	void OnCollisionStay2D (Collision2D other) {
		if (other.gameObject.tag == Model.Trap || other.gameObject.tag == Model.Enemy) {
			Debug.Log ("name :" + other.gameObject.tag);
			controller.playerDie ();
		}

	}
}