﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowerController : MonoBehaviour {
	#region Component 
	public Transform camPos;
	public Transform player;
	public Camera cam;
	#endregion
	public float number;
	#region  Variable
	public bool fixHorizontal;
	public bool fixVertical;
	public float speed = 1;
	#endregion

	#region Vector3 Add Position
	public float HorizontalAdd = 0;
	public float VerticalAdd = 0;
	#endregion

	#region private
	private float posX;
	private float posY;
	#endregion
	// Update is called once per frame
	public void cameraFollower () {
		if (fixHorizontal == false) {
			posX = player.position.x + HorizontalAdd;
		} else {
			posX = camPos.position.x - VerticalAdd;
		}
		if (fixVertical == false) {
			posY = player.position.y;
		} else {
			posY = camPos.position.y;
		}
		camPos.position = Vector3.Lerp (camPos.position, new Vector3 (posX, posY, -1), speed * Time.deltaTime);
	}

	public void changeScale (bool animated, float scaleDes, float speed) {
		cam.orthographicSize = Mathf.Lerp (cam.orthographicSize, scaleDes, speed * Time.deltaTime);
	}
}