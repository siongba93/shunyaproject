﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CanvasScalerController : MonoBehaviour
{
    public CanvasScaler[] canvas;
    void Start()
    {
        foreach (var myCanvas in canvas)
        {
            myCanvas.referenceResolution = new Vector2(Screen.width, 768);
        }
    }
}
