﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCameraHorizontalController : MonoBehaviour {

	public Vector3 Pos;
	public CameraController cameraController;
	#region main controller

	void Start () {
		cameraController = GameObject.FindWithTag ("MainCamera").GetComponent<CameraController> ();
	}

	void setCameraController (CameraController camController) {
		cameraController = camController;
	}
	#endregion

	void OnTriggerStay2D (Collider2D obj) {
		if (obj.gameObject.tag == "Player") {
			cameraController.setCamera (Pos);
		}
	}
}