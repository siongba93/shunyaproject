﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeCameraController : MonoBehaviour {
	public CameraController cameraController;
	public CameraBlockState state;
	#region main controller

	void Start () {
		cameraController = GameObject.FindWithTag ("MainCamera").GetComponent<CameraController> ();
	}

	void setCameraController (CameraController camController) {
		cameraController = camController;
	}

	#endregion
	void OnTriggerStay2D (Collider2D obj) {
		if (obj.gameObject.tag == "Player") {
			switch (state) {
				case CameraBlockState.HORIZONTAL:
					Debug.Log ("Free Horizontal");
					cameraController.freeHorizontalCamera ();
					break;
				case CameraBlockState.VERTICAL:
					Debug.Log ("Free Vertical");
					cameraController.freeVerticalCamera ();
					break;
				case CameraBlockState.ALL:
					Debug.Log ("Free ALL");
					cameraController.freeAllCamera ();
					break;
				default:
					break;
			}
		}
	}
}