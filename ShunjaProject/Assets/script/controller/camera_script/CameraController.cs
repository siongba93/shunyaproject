﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDataManager;

public class CameraController : PlayerStateController
{
    [Header("Init Component")]
    public Transform Player = null;

    [Header("Camera Horizontal/Camera Vertical")]
    #region Camera Horizontal/Camera Vertical
    public Vector3 targetPos = Vector3.zero;
    public Vector3 delta = Vector3.zero;
    public float boundX = 2.0f;
    public float boundY = 1.5f;
    public float speedX = 0;
    public float speedY = 0;
    #endregion

    [Header("cameraVerticalIsGround")]
    #region cameraVerticalIsGround
    public float boundYGround = 1.5f;
    public float IsGroundSpeed = 0.5f;
    #endregion

    [Header("setCamera / FreeCamera")]
    #region setCamera / FreeCamera
    public Vector3 blockCameraPos = Vector3.zero;
    public bool cameraBlockHorizontal = false;
    public bool cameraBlockVertical = false;
    public float blockSpeed = 0;
    #endregion

    [Header("CameraScale")]
    #region CameraScale
    public int Distance;
    public float speed = 1;
    public int MaxDistance = 15, MinDistance = 3;
    #endregion

    #region main controller

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (GameModel.gameStart == true)
        {
            delta = Vector3.zero;
            cameraHorizontal();
            cameraVerticalIsGround();
            cameraVertical();
            SetCameraPosition();
            //	CameraScale ();
        }
    }

    public void setCamera(Vector3 fixedPos)
    {
        cameraBlockHorizontal = true;
        cameraBlockVertical = true;
        blockCameraPos = fixedPos;
    }

    public void freeHorizontalCamera()
    {
        cameraBlockHorizontal = false;
    }

    public void freeVerticalCamera()
    {
        cameraBlockVertical = false;
    }

    public void fixAllCamera()
    {
        cameraBlockHorizontal = true;
        cameraBlockVertical = true;
    }

    public void freeAllCamera()
    {
        cameraBlockHorizontal = false;
        cameraBlockVertical = false;
    }

    #endregion

    #region main API

    void cameraHorizontal()
    {
        float dx = Player.position.x - transform.position.x;
        if (dx > boundX || dx < -boundX)
        {
            // check if over Green color
            if (transform.position.x < Player.position.x)
            {
                delta.x = dx - boundX;
            }
            // check if over Purple color
            else
            {
                delta.x = dx + boundX;
            }
        }
    }

    void cameraVerticalIsGround()
    {
        float dy = Player.position.y - transform.position.y;
        if (dy > boundYGround || dy < -boundYGround)
        {
            // check if over Blue Color Line 
            if (transform.position.y < Player.position.y)
            {
                if (PlayerIsGround)
                {
                    delta.y = dy - boundYGround;
                    speedY = IsGroundSpeed;
                }
            }
            // check if over Red Color Line 
            else if (transform.position.y > Player.position.y)
            {
                delta.y = dy + boundYGround;
                speedY = speedX;
            }
        }
    }

    void cameraVertical()
    {
        float dy = Player.position.y - transform.position.y;
        // check if Over Purple Color Line
        if (dy > boundY || dy < -boundY)
        {
            if (transform.position.y < Player.position.y)
            {
                delta.y = dy - boundY;
            }
        }
    }

    void SetCameraPosition()
    {
        targetPos = transform.position + delta;
        float PosX = transform.position.x;
        PosX = Mathf.Lerp(PosX, targetPos.x, speedX);
        float PosY = transform.position.y;
        PosY = Mathf.Lerp(PosY, targetPos.y, speedY);
        if (cameraBlockHorizontal == true && cameraBlockVertical == false)
        {
            Debug.Log("1");
            transform.position = new Vector3(blockCameraPos.x, PosY, transform.position.z);
        }
        else if (cameraBlockHorizontal == false && cameraBlockVertical == true)
        {
            Debug.Log("2");
            transform.position = new Vector3(PosX, blockCameraPos.y, transform.position.z);
        }
        else if (cameraBlockHorizontal == false && cameraBlockVertical == false)
        {
            Debug.Log("3");
            transform.position = new Vector3(PosX, PosY, transform.position.z);
        }
        else if (cameraBlockHorizontal == true && cameraBlockVertical == true)
        {
            // transform.position = blockCameraPos;
            transform.position = Vector3.MoveTowards(transform.position, blockCameraPos, blockSpeed * Time.deltaTime);
        }
    }

    // void CameraScale () {
    // 	if (Camera.main.orthographicSize >= 5) {
    // 		/*MouseWheel*/
    // 		if ((Input.GetAxis ("Mouse ScrollWheel") > 0) && Camera.main.orthographicSize > MinDistance) {
    // 			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, -Distance, speed * Time.deltaTime);

    // 		} else if ((Input.GetAxis ("Mouse ScrollWheel") < 0) && Camera.main.orthographicSize < MaxDistance) {
    // 			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, Distance, speed * Time.deltaTime);

    // 		} else if (Input.GetMouseButton (2)) {
    // 			Camera.main.orthographicSize = Mathf.Lerp (Camera.main.orthographicSize, MaxDistance / 4, 10 * speed * Time.deltaTime);

    // 		}
    // 		/*End*/

    // 	} else {
    // 		Camera.main.orthographicSize = 5f;
    // 	}
    // }

    #endregion
}
