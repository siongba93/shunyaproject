﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScreenScaler : MonoBehaviour
{
    public Camera cameraScreen;
    void Start()
    {
        Debug.Log(Screen.width);
        switch (Screen.width)
        {
            case 1024:
            case 800:
            case 640:
                cameraScreen.orthographicSize = 2.7f;
                break;
            case 1366:
            case 1280:
                cameraScreen.orthographicSize = 2;
                break;
            default:
                break;
        }
    }
}
