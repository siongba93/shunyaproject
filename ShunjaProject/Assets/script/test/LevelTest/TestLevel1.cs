﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLevel1 : MonoBehaviour
{
    #region Controller
    public FakeGameLevelManager levelManagerController;
    public PlayerMainController playerController;
    public CameraController cameraController;
    #endregion

    public Vector2 resetPosition;

    void Start()
    {
        playerController.setResetPosition(resetPosition);
        cameraController.fixAllCamera();
        levelManagerController.gameStart();
    }

}
