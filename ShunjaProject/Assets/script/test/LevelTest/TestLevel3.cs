﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLevel3 : MonoBehaviour {
	public FakeGameLevelManager levelManagerController;
	public PlayerMainController playerController;

	public Vector2 resetPosition;

	void Start () {
		levelManagerController.gameStart ();
		playerController.setResetPosition (resetPosition);
	}
}