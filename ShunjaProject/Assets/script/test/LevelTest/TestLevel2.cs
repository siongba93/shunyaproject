﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLevel2 : MonoBehaviour {
	#region Controller
	public FakeGameLevelManager levelManagerController;
	public PlayerMainController playerController;
	#endregion

	public Vector2 resetPosition;

	void Start () {
		levelManagerController.gameStart ();
		playerController.setResetPosition (resetPosition);
	}
}
