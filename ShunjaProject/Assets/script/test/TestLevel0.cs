﻿using System.Collections;
using System.Collections.Generic;
using UnityCursorControl;
using UnityEngine;
using UnityEngine.UI;

public class TestLevel0 : MonoBehaviour {

	[SerializeField]
	private Text _globalPosText;
	[SerializeField]
	private Text _localPosText;

	public DrawSquareController DrawController;
	public PlayerMovementController Player;
	public CameraController Camera;

	void Update () {
		//		Player.setPlayerMovement ();
		//		DrawController.SetMouseController ();
		//		Camera.cameraMovement ();
		//		UpdatePositionText ();
	}

	void FixedUpdate () { }

	private void UpdatePositionText () {
		_localPosText.text = "Local Cursor Position: " + ((Vector2) Input.mousePosition).ToString ();
	}
}