﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;
            yield return null;
        }

        audioSource.Stop();
        audioSource.volume = startVolume;
    }

    public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime)
    {
        float endVolume = audioSource.volume;
        audioSource.Play();
        while (audioSource.volume < 1)
        {
            audioSource.volume += 1 * Time.deltaTime / FadeTime;
            yield return null;
        }

        audioSource.volume = 1;
    }


    /// <param name="audioSource">Parameter Audio Source Component</param>
    /// <param name="myAudio">Parameter audio that want to play</param>
    /// <param name="volume">Parameter volume of the audio Source</param>
    /// <returns> Explanation :  Play Audio Source with specific audio</returns>
    public static void playAudioClip(AudioSource audioSource, AudioClip myAudio, float volume = 1)
    {
        audioSource.volume = volume;
        audioSource.clip = myAudio;
        audioSource.Play();
    }
}
